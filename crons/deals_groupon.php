<?php

include_once '../functions/db.connect.inc';
include('../class/MyLogPHP.class.php');
$log = new MyLogPHP();
/* ini_set('display_errors',1); */

###### Parameters ######
$affiliateID = 204403;
$mediaID = 212556;
$tsToken = urlencode("US_AFF_0_204403_212556_0");
$limit = 250;

function checkDeals($dealId) {
	//check the deals exist or not
	global $db_pdo;
	$sql = $db_pdo->prepare("select * FROM `deals` where `deal_id` = :deal_id");
	$sql->execute(array(':deal_id' => $dealId));
	if ($sql->rowCount() > 0) {
		return false;
	} else {
		return true;
	}
}

$log->info('DEALS COUPON INTERNATIONAL STARTED');

$date = date("Y-m-d h:i:s");

$cronDate = date('N', strtotime($date));

$locationQuery = $db_pdo->prepare("SELECT * from countryCodes where index_ref=:indexRef");
$locationQuery->execute(array(':indexRef'=>$cronDate));
$locationRows = $locationQuery->fetchAll();
//Call API for each location
for ($i = 0; $i < count($locationRows); $i++) {
	$currentLocation = $locationRows[$i];
	$locationID = $currentLocation['id'];
	$cityID = $currentLocation['country_code'];
	//Make a API call to retrieve the total of records available (limit=1)
	$url = "https://partner-int-api.groupon.com/deals.json?tsToken=$tsToken&country_code=$cityID&limit=1";
	$jsonResponse = file_get_contents($url);
	$rows = json_decode($jsonResponse, true);
	//Total Records
	
	$totalRecords = $rows['pagination']['count'];
	$log->info($totalRecords,'TOTAL RECORDS CHECKED');
	//Number of calls (each API Call can retrieve 250 rows max)
	$pagination = ceil($totalRecords / 250);
	for ($p = 0; $p < $pagination; $p++) {
		//Calculate offset
		$limit = 250;
		$offset = $p * 250;
		$accessURL = "https://partner-int-api.groupon.com/deals.json?country_code=$cityID&tsToken=$tsToken&offset=$offset";
		$retrievedData = file_get_contents($accessURL);
		$data = json_decode($retrievedData, true);
		$dealsRow = $data['deals'];
		//Process the result
		foreach ($dealsRow as $deals) {
			$dealDetails = $deals['options'][0];
			/* merchant */
			$merchant    = $deals['merchant'];
			$store_id    = $merchant['id'];
			$dealID    = $dealDetails['id'];
			$shortAnnouncementTitle = $deals['shortAnnouncementTitle'];
			$dealUrl    = $deals['dealUrl'];
			$deal_source   = "";
			$title    = $dealDetails['title'];
			$description   = $dealDetails['details'][0]["description"];
			$expiresAt    = $dealDetails['expiresAt'];
			$expiresAt    = date("Y-m-d H:i:s", strtotime($expiresAt));
			$startAt    = $deals['startAt'];  //Note: StartDate is Post Date? YES
			$startAt    = date("Y-m-d H:i:s", strtotime($startAt));
			//$show_image_url  = "";
			$smallImageUrl   = $deals['smallImageUrl'];
			$logo_url    = "";
			$largeImageUrl   = $deals['largeImageUrl'];
			$show_image_url             = $deals['largeImageUrl'];
			$provider_name   = $merchant['name'];
			$category_id   = "";
			if (!empty($deals['channels'])) {
				$category   = $deals['channels'][0]['name'];
			} elseif (!empty($deals['tags'])) {
				$category   = $deals['tags'][0]['name'];
			}
			else
				$category    = "";
			$subcategoryID   = "";
			$subcategory   = "";
			/* price data */
			$priceData    = $dealDetails['price'];
			$price     = $priceData['amount'];
			$currency   = $priceData['currencyCode'];
			$valueData    = $dealDetails['value'];
			$value     = $valueData['amount'];
			$dealSavings   = "";
			$discountPercent            = $dealDetails['discountPercent'];
			$soldQuantity  = $dealDetails['soldQuantity'];
			/* division */
			$divisions    = $deals['division'];
			if($divisions['country'] !== NULL) {
				$country_code  = $divisions['country'];
			}else{
				$country_code = 'US';
			}
			$divisionID   = $divisions['id'];
			//print_r($country_code);
			/* locations */
			$locations    = $deals['locations'];
			if(isset($locations)){
				$locationName   = $locations[0]['name'];
				$latitude    = $locations[0]['lat'];
				$longitude   = $locations[0]['lng'];
				$city    = $locations[0]['city'];
				$streetAddress1  = $locations[0]['streetAddress1'];
				$postCode    = $locations[0]['postalCode'];
			}


			if(!empty($cityID) &&
				!empty($show_image_url) &&
				!empty($country_code) &&
				!empty($category) &&
				!empty($currency) &&
				!empty($price) &&
				!empty($discountPercent) ){
				$paramsArray = array(
					':store_id'   => $store_id,
					':deal_id'    => $dealID,
					':deal_name'   => $shortAnnouncementTitle,
					':deal_url'   => $dealUrl,
					':location_name' => $locationName,
					':location_address' => $streetAddress1,
					':location_city'  => $city,
					':city_id'   => $cityID,
					':location_postcode'=> $postCode,
					':deal_source'   => $deal_source,
					':deal_title'   => $title,
					':description'   => $description,
					':expiration_date'  => $expiresAt,
					':post_date'   => $startAt,
					':show_image_url'  => $show_image_url,
					':image_small_url'  => $smallImageUrl,
					':image_large_url'      => $largeImageUrl,
					':logo_url'   => $logo_url,
					':provider_name'  => $provider_name,
					':category_id'   => $category_id,
					':category'   => $category,
					':sub_category_id'  => $subcategoryID,
					':subcategory'   => $subcategory,
					':lat'     => $latitude,
					':lon'     => $longitude,
					':total_sold'  => $soldQuantity,
					':org_price'   => $value,
					':price'    => $price,
					':savings'    => $dealSavings,
					':discount_percent' => $discountPercent,
					':currency'   => $currency,
					':country_code'  => $country_code,
					':division_id'  => $divisionID,
					':deal_site_id'  => 1);

				if (checkDeals($dealID)) {
					//Create deal if it does not exit
					$insertSql = $db_pdo->prepare("INSERT INTO deals (
                			store_id,
							deal_id,
							deal_name,
							deal_url,
                			location_name,
                			location_address,
                			location_city,
                			city_id,
                            location_postcode,
                            deal_source,
                            deal_title,
                            description,
                            expiration_date,
                            post_date,
                            show_image_url,
                            Image_small_url,
                            logo_url,
                            provider_name,
                            category_id,
                            category,
                            sub_category_id,
                            subcategory,
                            lat,
                            lon,
                            total_sold,
                            org_price,
                            price,
                            savings,
                            discount_percent,
                            currency,
                            country_code,
                            division_id,
                            deal_site_id
                            )
					VALUES (:store_id,
                            :deal_id,
                            :deal_name,
                            :deal_url,
                            :location_name,
                            :location_address,
                            :location_city,
                            :city_id,
                            :location_postcode,
                            :deal_source,
                            :deal_title,
                            :description,
                            :expiration_date,
                            :post_date,
                            :show_image_url,
                            :image_small_url,
                            :image_large_url
                            :logo_url,
                            :provider_name,
                            :category_id,
                            :category,
                            :sub_category_id,
                            :subcategory,
                            :lat,
                            :lon,
                            :total_sold,
                            :org_price,
                            :price,
                            :savings,
                            :discount_percent,
                            :currency,
                            :country_code,
                            :division_id,
                            :deal_site_id)");

					$insertSql->execute($paramsArray);
					//print_r($insertSql->errorInfo());

				} else {
					//Update an existing deal
					$updateSql = $db_pdo->prepare("UPDATE deals SET
                			store_id=:store_id,
                			deal_id=:deal_id,
                			deal_name=:deal_name,
							deal_url=:deal_url,
                            location_name = :location_name,
                            location_address=:location_address,
                            location_city=:location_city,
                            city_id=:city_id,
                            location_postcode=:location_postcode,
							deal_source=:deal_source,
                            deal_title=:deal_title,
                            description=:description,
                            expiration_date=:expiration_date,
                            post_date=:post_date,
                            show_image_url=:show_image_url,
                            image_small_url=:image_small_url,
                            image_large_url=:image_large_url,
                            logo_url=:logo_url,
                            provider_name=:provider_name,
                            category_id=:category_id,
                            category=:category,
                            sub_category_id=:sub_category_id,
                            subcategory=:subcategory,
                            lat=:lat,
                            lon=:lon,
                            total_sold=:total_sold,
                            org_price=:org_price,
                            price=:price,
                            savings=:savings,
                            discount_percent=:discount_percent,
							currency=:currency,
                            country_code=:country_code,
                            division_id=:division_id,
                            deal_site_id = :deal_site_id
                            WHERE deal_id=:deal_id");


					$updateSql->execute($paramsArray);

				}

			}
		}
	}
}
	
	global $db_pdo;	
	
	$sql = $db_pdo ->prepare("select count(id) FROM `deals`");
	
	$sql->execute();
	
	$number_of_rows = $sql->fetchColumn();     	
	
$log->info($number_of_rows,'TOTAL RECORDS IN THE DB');

$log->info('DEALS COUPON INTERNATIONAL ENDED');

?>
