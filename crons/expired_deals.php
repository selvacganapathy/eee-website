<?php

ini_set('display_errors',1);

require '../functions/db.connect.inc';
include('../class/MyLogPHP.class.php');
$log = new MyLogPHP();


function deleteExpiredDeals() {
	
	global $db_pdo;
	
	$sql = "DELETE FROM `deals` WHERE expiration_date < CURDATE()";

	$query = $db_pdo->prepare($sql);

	$query->execute();
	
	$count = $query->rowCount();
	
	$log = new MyLogPHP();
	
	$log->info($count, 'ROWS AFFECTED');
	
	

} 

function writeCSV() {
	
	global $db_pdo;
	
    $sql = $db_pdo->prepare("SELECT * FROM deals WHERE expiration_date < CURDATE()");

    $sql->execute();
    
	$row = $sql->fetchAll();
	
	$date = date("Y-m-d h:i:s");
  
	$fp = fopen( '../expiredDeals_csv/'.$date.'.csv', 'w');

	foreach ($row as $arrayElement) {
	                
                fputcsv($fp, $arrayElement);

    }
    
    fclose($fp);
}
$log->info('DELETING....');

writeCSV();
deleteExpiredDeals();
$log->info('DELETED....');

?>
