<?php

ini_set('display_errors',1);

include_once '../functions/db.connect.inc';

include '../class/MyLogPHP.class.php';

function getCategory() 
{
	
	global $db_pdo;
	
	$rows = json_decode(file_get_contents('http://api.8coupons.com/v1/getcategory'),true);
	
	$sql = "INSERT INTO categories (category_id,category_name) VALUES(:categoryID, :categoryName) 
	ON DUPLICATE KEY UPDATE 
	category_id = VALUES(category_id),category_name= VALUES(category_name)";

	$query = $db_pdo->prepare($sql);
	
	foreach ($rows as $data) {
				
			$query->execute(array(':categoryID'=>$data['categoryID'] ,':categoryName'=>$data['category']));
			
	
	}
}

function GetLocations() 
{

global $db_pdo;

 	$sql = "SELECT country_code FROM countryCodes";
 
	foreach($db_pdo->query($sql) as $row) 
	{
	 	 $countryCode = $row['country_code'];
		 @$decodeXML = simplexml_load_file('http://api.groupon.de/api/v1/cities/'.$countryCode,'SimpleXMLElement',LIBXML_NOWARNING);
		
		
		foreach(@$decodeXML as $data) 
		{
		$city_id 		= $data['id'];
		$city_name		= $data['name'];
		$latitude		= $data['latitude'];	
		$longitude		= $data['longitude'];	
		$city_gro_id	= $data['city_group_id'];
		$country_code	= $data->country['shortname'];
		
		
		if(@$decodeXML)
			{
	
			
			$sql1 = "INSERT INTO locations (country_code,city_id,city_name,lat,lng,city_group_id) VALUES(:country_code, :city_id,:city_name,:lat,:lng,:city_group_id)
			
			ON DUPLICATE KEY UPDATE
			
			country_code=VALUES(country_code),city_id=VALUES(city_id),city_name=VALUES(city_name),lat=VALUES(lat),lng=VALUES(lng),city_group_id=VALUES(city_group_id)
			
			";
	
			
			$query = $db_pdo->prepare($sql1);
		
				foreach ($data as $rows) 
				{
							
				$query->execute(array(':country_code'=>$data->country['shortname'] ,':city_id'=> $data['id'],
										':city_name'=>$data['name'] ,':lat'=>$data['latitude'],
										':lng'=>$data['longitude'] ,':city_group_id'=>$data['city_group_id']));
																	
				echo $city_id.','.$city_name.','.$latitude.','.$longitude.','.$city_gro_id.','.$country_code.'<br>';
				
		
				}
	
			}
			else
			{
				echo "Update Fail";
			}
		}
	}
}

GetLocations();

?>