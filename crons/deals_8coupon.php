<?php
ini_set('display_errors',1);

require '../functions/db.connect.inc';

include '../class/MyLogPHP.class.php';

$log = new MyLogPHP();

$log->info('-----LOG STARTED -'.date('M').'---');

function countryCode($country) {

	global $db_pdo;
		
    $sql = $db_pdo->prepare("SELECT country_code FROM countryCodes WHERE country_name=:country");

    $sql->execute(array(':country' => $country));
    
	$row = $sql->fetchAll();
   
	foreach ($row as $arrayElement) {

            return $arrayElement['country_code'];
    }
    return false;


}

function getLocation($lat,$lon) {

	$country= null;
	
	$url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=".$lat.",".$lon."&sensor=true";
    $data = @file_get_contents($url);
    $jsondata = json_decode($data,true);
    // country
	foreach ($jsondata["results"] as $result) {
    	foreach ($result["address_components"] as $address) {
        	if (in_array("country", $address["types"])) {
            $country = $address["long_name"];
            
			}
		}
	}
	return countryCode($country);
}



function checkDeals($dealId) {
	//check the deals exist or not 
	global  $db_pdo;
	
	$sql = $db_pdo ->prepare("select count(deal_id) FROM `deals` where `deal_id` = :deal_id");
	
	$sql->execute(array(':deal_id' => $dealId));
	
	$number_of_rows = $sql->fetchColumn(); 

	if($number_of_rows > 0)
	{	
		return false;		
	}
	else
	{
		return true;
	}
}


function category($categoryID) {

	global $db_pdo;
	
    $sql = $db_pdo->prepare("SELECT category_name FROM categories WHERE category_id=:category_id");

    $sql->execute(array(':category_id' => $categoryID));
    
	$row = $sql->fetchAll();
   
	foreach ($row as $arrayElement) {

            return $arrayElement['category_name'];
    }
    return false;


}

function getData() {
	
	global $db_pdo;
	
	$category = array();
	
	$rows = json_decode(file_get_contents('http://api.8coupons.com/v1/getrealtimelocaldeals/?key=82cb2645283a89fe53352968298a7fd61ad8386f5110836008c14e1435faf5b3a8f063a5ce613aa22b528c095b429ee2'),true);
	
	$rows = array_filter($rows);	
		
	
		foreach($rows as $data) {
			
			$category = category($data['categoryID']);
			
			$countryCode = getLocation($data['lat'],$data['lon']);
			
			$sql = $db_pdo -> prepare("INSERT INTO deals (store_id,deal_id,deal_name,deal_url,deal_source,deal_title,description,expiration_date,post_date,show_image_url,
			Image_small_url,logo_url,provider_name,category_id,category,sub_category_id,lat,lon,org_price,price,savings,discount_percent,location_city,
            location_name,location_address,location_postcode,deal_site_id,country_code,currency) 
			
			VALUES 
			
			(:store_id,:deal_id,:deal_name,:deal_url,:deal_source,:deal_title,:description,:expiration_date,:post_date,:show_image_url,
			:image_small_url,:logo_url,:provider_name,:category_id,:category,:sub_category_id,:lat,:lon ,:org_price,:price,:savings,:discount_percent,
			:location_city,:location_name,:location_address,:location_postcode,:deal_site_id,:country_code,:currency)
			
			ON DUPLICATE KEY UPDATE 
			
			store_id=VALUES(store_id),deal_id=VALUES(deal_id),deal_name=VALUES(deal_name),deal_url=VALUES(deal_url),deal_source=VALUES(deal_source),
			deal_title=VALUES(deal_title),description=VALUES(description),expiration_date=VALUES(expiration_date),post_date=VALUES(post_date),
			show_image_url=VALUES(show_image_url),Image_small_url=VALUES(Image_small_url),logo_url=VALUES(logo_url),provider_name=VALUES(provider_name),
			category_id=VALUES(category_id),category=VALUES(category),sub_category_id=VALUES(sub_category_id),lat=VALUES(lat),lon=VALUES(lon),org_price=VALUES(org_price),
			price=VALUES(price),savings=VALUES(savings),discount_percent=VALUES(discount_percent),location_city=VALUES(location_city),location_name=VALUES(location_name),
			location_address=VALUES(location_address),location_postcode=VALUES(location_postcode),deal_site_id=VALUES(deal_site_id),country_code=VALUES(country_code),currency=VALUES(currency)");
			
			$sql -> execute(array(':store_id' 		=> $data['storeID'],
								  ':deal_id' 		=> $data['ID'],
								  ':deal_name' 		=> $data['name'],
								  ':deal_url'		=> $data['URL'],
								  ':deal_source' 	=> "",
								  ':deal_title' 	=> $data['dealTitle'],
								  ':description' 	=> $data['dealinfo'],
								  ':expiration_date'=> $data['expirationDate'],
								  ':post_date' 		=> $data['postDate'],
								  ':show_image_url' => $data['showImage'],
								  ':image_small_url'=> $data['showImageStandardSmall'],
								  ':logo_url' 		=> "",
								  ':provider_name' 	=> $data['providerName'],
								  ':category_id' 	=> "",
								  ':category' 		=> $category,
								  ':sub_category_id'=> "",
								  ':lat' 			=> $data['lat'],
								  ':lon' 			=> $data['lon'],
								  ':org_price' 		=> $data['dealOriginalPrice'],
								  ':price' 			=> $data['dealPrice'],
								  ':savings' 		=> $data['dealSavings'],
								  ':discount_percent'	=> $data['dealDiscountPercent'],
								  ':location_city' 		=> $data['city'],
								  ':currency'			=> 'USD',
 					              ':location_name'		=> $data['name'],
					              ':location_address'	=> $data['address'],
					              ':location_postcode'	=> $data['ZIP'],
					              ':deal_site_id'		=> 2,
					              ':country_code'		=> $countryCode));
			$Category[] = $data;		
		}
		
		return $Category;
		
}

getData();



$log->info('-----LOG ENDED---');
?>

