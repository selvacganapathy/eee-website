<?php

require 'db.connect.inc';

function categoryList($country_code) {
	
	global $db_pdo;
	
	if($country_code == ''){
		return ;
	}
	$sql = "select DISTINCT category from deals WHERE country_code='$country_code'";
		
	$sth = $db_pdo->query($sql);
	
	foreach($sth as $row) {
	
	$result = $row['category'];
 
		if($result !== '') 
		{
			echo "<a href='?country=".$country_code."&category=".$result."' class='list-group-item'>".$result."</a>";
		}
	}
}

function getCountryFromCountryCode($countryCode) {
	
	global $db_pdo;
	
	$sql = "SELECT country_name FROM countryCodes WHERE country_code ='$countryCode'";
		
	$sth = $db_pdo->query($sql);
	
	foreach($sth as $row) {
	
	$result = $row['country_name'];
 
		if($result !== '') 
		{
			echo "<a href=?country=".$countryCode." class='list-group-item'>".$result."</a>";
		}
	}

	
}
	
function countryDropDown() {
	
	global $db_pdo;
	
	$sql = "SELECT DISTINCT country_code FROM deals";
		
	$sth = $db_pdo->query($sql);
	
	foreach($sth as $row) {
	
	$result = $row['country_code'];
 
	$countryName = getCountryFromCountryCode($result);
		
		if($result !== '') 
		{
			echo "<a href='#'>".$countryName."</a>";
		}
	}
}

function displayDeals($country,$category) {
	
	
	global $db_pdo;
	
	$sql = "SELECT * FROM deals WHERE country_code='$country' AND category='$category'";
		
	$sth = $db_pdo->query($sql);
	
	foreach($sth as $row) {
	
	$dealName 	= $row['deal_name'];
	$dealTitle	= $row['deal_title'];
	$description= $row['description'];
	$startDate 	= $row['post_date'];
	$enddate	= $row['expiration_date'];
	$imageUrl	= $row['image_small_url'];
	$price		= $row['price'];
	$discount	= $row['discount_percent'];
	$original_price = $row['org_price'];
		if($dealTitle !== '') 
		{
			echo "<div class='col-sm-4 col-lg-4 col-md-4'>";
                        echo "<div class='thumbnail>";
                           echo "<img src='".$imageUrl."' alt='No-Image'>";
                            echo "<div class='caption'>";
                                echo "<h4 class='pull-right'>".$dealTitle."</h4>";
                                echo "<h4><a href='#'>".$dealName."</a></h4>";
                                echo "<p>".$description."</p>";
                            	echo "</div>";
                            	echo "<div class='ratings'>";
                                echo "<p class='pull-right'><b>Offer Price:</b> ".$price."</p>";
                                echo "<p class='pull-right'><b>Original Price:</b>".$original_price."</p>";
                                echo "<p>
                                     <b>Discount :</b> $discount %</p>";
                                 
                            echo "</div></div></div>";

		}
	}
	return;
}

?>