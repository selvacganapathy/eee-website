<?php

require 'db.connect.inc';

function deleteExpiredDeals() {
	
	global $db_pdo;
	
	$sql = "DELETE FROM `deals` WHERE expiration_date < CURDATE()";

	$query = $db_pdo->prepare($sql);

	$query->execute();
} 

?>