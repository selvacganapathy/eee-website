<?php
ini_set('display_errors',1);
ini_set('memory_limit', '-1');
require '../functions/db.connect.inc';
require '../functions/displayFunctions.inc';
require '../functions/functions.inc';

function countDeals() {
	

	global $db_pdo;	
	
	$sql = $db_pdo ->prepare("select count(id) FROM `deals`");
	
	$sql->execute();
	
	$number_of_rows = $sql->fetchColumn();     	

	//echo ('<h4>Total Number of deals until '. date('Y-m-d H:i:s').'  =  ' .$number_of_rows. '</h4>');
	
	$json=json_encode($number_of_rows);
	
	print_r('<h4>Total Number of deals until '. date('Y-m-d H:i:s').'  =  ' .$json. '</h4>');

}	
function recordsToday() {
	
	global $db_pdo;	
	
		$sql = $db_pdo ->prepare("SELECT count(id) FROM `deals` WHERE DATE(`created_at`) = CURDATE()");
	
	$sql->execute();
	
	$number_of_rows = $sql->fetchColumn();     	
	
	$json=json_encode($number_of_rows);
	
	print_r('<h4>Total Number of deals today at '. date('Y-m-d H:i:s').'  =  ' .$json. '</h4>');
	
}

function displayAllDeals() {
	
	global $db_pdo;	
	
	$sql = $db_pdo ->prepare("SELECT * FROM `deals` LIMIT 5000");
	
	$sql->execute();
	
	$result = $sql->fetchAll();
	echo '<pre>';
	print_r($result);
	echo '</pre>';
}


countDeals();
recordsToday();
displayAllDeals();

?>