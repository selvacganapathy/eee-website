<div class="stripe-white big items-carousel-wrap">
    <div class="row">
        <div class="column">
            <h2><?php _e('Popular Coupons', 'Couponize');?></h2>
        </div>
    </div>
    <div class="row collapse">
        <div class="column">
            <div class="items-carousel flexslider">
                <ul class="rr slides">
                    <?php 
                    global $db_pdo;
                    $sth_popular = $db_pdo->prepare("SELECT DISTINCT  * FROM `deals` ORDER BY total_sold DESC LIMIT 10");
	            $sth_popular->execute();
	            $result_popular = $sth_popular->fetchAll();
		     foreach($result_popular as $row) {
                        $discount_popular = $row['discount_percent'];
                        $date_popular= strtotime($row['expiration_date']);
                        $code_popular=$row['deal_id'];
                        $url_popular = $row['deal_url'];   
                        $categories_popular = $row['category'];
                        $category_id_popular = $row['category_id'];
                        $thumb_popular = $row['show_image_url'];
                        $title_popular=$row['deal_title'];
                        $shortTitle_popular=$row['deal_name'];
                        $price_popular=$row['price']/100;
                        $currency_popular=$row['currency'];
                            
                            $fmt = numfmt_create( 'de_DE', NumberFormatter::CURRENCY );
							$price_formatted = numfmt_format_currency($fmt, $price_popular,$currency_popular);

                        $daysleft_popular = round( ($date_popular-time()) / 24 / 60 / 60);
                        $dealDetailsUrl=home_url("/")."?coupons=coupon-details&dealerID=$code_popular";
//                        ?>
                        <li>
                            <div class="wrapper-3 item-thumb">
                                <div class="top">
                                    <figure>
                                        <a href="<?php echo  get_category_link( $category_id_popular ) ;?>">
                                            <img src="<?php echo $thumb_popular ?>" width="200" height="110" alt="<?php echo $title_popular;?>">
                                        </a>
                                    </figure>
                                    <h2 class="alt"><a href="<?php echo $dealDetailsUrl;?>"><?php echo substr($shortTitle_popular, 0, 37);?></a></h2>
                                </div>
                                <div class="bottom">
                                    <?php if($discount_popular != '') { ?>
                                        <p class="value secondary"><?php echo $discount_popular."%";?></p>
                                    <?php } ?>
                                        <h6>
                                            <?php 
                                            if($date_popular == '') 
                                                _e('VALID', 'Couponize');
                                            else if($daysleft_popular <= 0) 
                                                _e('EXPIRED', 'Couponize'); 
                                            else echo sprintf( _n('%d day left.', '%d days left.', $daysleft_popular, 'Couponize'), $daysleft_popular ); ?>
                                        </h6>
                                    <h6 class="value secondary"><?php echo $price_formatted;?></h6>
                                    <a href="<?php echo $dealDetailsUrl;?>" class="input button red secondary"><?php _e('Learn more', 'Couponize');?></a>
                                </div>
                            </div>
                        </li> 
                     <?php };?>
                </ul>
            </div>
        </div>
    </div>
</div>