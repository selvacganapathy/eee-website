<?php
// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die (esc_attr_e('Please do not load this page directly. Thanks!','Couponize'));

	if ( post_password_required() ) { ?>

<p class="nocomments"><?php esc_attr_e('This post is password protected. Enter the password to view comments.','Couponize') ?></p>
<?php
		return;
	}
?>
<!-- You can start editing here. -->

<section class="comments">

<?php if ( have_comments() ) : ?>
	
		<div class="row">
	        <div class="large-8 columns">
				<h2><?php comments_number(esc_attr__('No comments','Couponize'), esc_attr__('One comment','Couponize'), '% '.esc_attr__('comments','Couponize') );?></h2>
	        </div>
	        <div class="large-4 columns">
	            <div class="post-comment-wrap">
	            	<a href="#respond" class="post-comment-link"><?php _e('Post a comment', 'Couponize');?></a>
	            </div>
	        </div>
	    </div>
			
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<div class="paginate comment-paginate clearfix">
				<div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Older Comments', 'Couponize' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( __( 'Newer Comments <span class="meta-nav">&rarr;</span>', 'Couponize' ) ); ?></div>
			</div> <!-- .navigation -->
		<?php endif; // check for comment navigation ?>
		<?php if ( ! empty($comments_by_type['comment']) ) : ?>
			<div class="wrapper-3 primary oh comments">
				<ol class="commentlist clearfix" id="comments">
					<?php wp_list_comments( array('type'=>'comment','callback'=>'couponize_comment') ); ?>
				</ol>
			</div>
		<?php endif; ?>
		
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<div class="paginate comment-paginate clearfix">
				<div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Older Comments', 'Couponize' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( __( 'Newer Comments <span class="meta-nav">&rarr;</span>', 'Couponize' ) ); ?></div>
			</div> <!-- .navigation -->
		<?php endif; // check for comment navigation ?>
			
		<?php if ( ! empty($comments_by_type['pings']) ) : ?>
			<div id="trackbacks">
				<h3 id="trackbacks-title"><?php esc_html_e('Trackbacks/Pingbacks','Couponize') ?></h3>
				<ol class="pinglist">
					<?php //wp_list_comments('type=pings&callback=tilability_pings'); ?>
				</ol>
			</div>
		<?php endif; ?>	
<?php else : // this is displayed if there are no comments so far ?>
   <div id="comment-section" class="nocomments">
      <?php if ('open' == $post->comment_status) : ?>
         <!-- If comments are open, but there are no comments. -->
         
      <?php else : // comments are closed ?>
         <!-- If comments are closed. -->
            <div id="respond">
               
            </div> <!-- end respond div -->
      <?php endif; ?>
   </div>
<?php endif; ?>

</section>

<?php if ('open' == $post->comment_status) : ?>
     <section class="post-comment">
        <div class="wrapper-3 primary">
        	<div class="row fields">
	<?php 
	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );
	if($commenter['comment_author'] != '') 
		$name = esc_attr($commenter['comment_author']);
	else 
		$name = '';
	if($commenter['comment_author_email'] != '') 
		$email = esc_attr($commenter['comment_author_email']);
	else
		$email = '';
	if($commenter['comment_author_url'] != '') 
		$url = esc_attr($commenter['comment_author_url']);
	else 
		$url = '';
	$fields =  array(
	'author' => '<div class="large-5 columns"><input class="input field primary" id="author" placeholder="Your name..." name="author" type="text" value="' . $name . '" ' . $aria_req . ' />',
	'email'  => '<input class="input field primary" id="email" placeholder="Your email..." name="email" type="text" value="' . $email . '" ' . $aria_req . ' />',
	'url'    => '<input class="input field primary" id="url" placeholder="Your website..." name="url" type="text" value="' . $url . '" /></div>'
	); 
	$class2 = 'large-7';
	if(is_user_logged_in() ) $class2 = 'large-12';
	$comment_textarea = '<div class="' . $class2 . ' columns"> <textarea class="input field primary" placeholder="Your Message..." id="comment" name="comment" aria-required="true"></textarea>  </div> <div style="clear: both"></div>';
	comment_form( array( 'fields' => $fields, 'comment_field' => $comment_textarea, 'id_submit' => 'contact_submit', 'label_submit' => esc_attr__( 'Submit Comment', 'Couponize' ), 'title_reply' => esc_attr__( 'Leave a Reply', 'Couponize' ), 'title_reply_to' => esc_attr__( 'Leave a Reply to %s', 'Couponize' )) ); ?>
	</div> <!-- closing the row for the submit button -->
</section>
<?php else: ?>

<?php endif; // if you delete this the sky will fall on your head ?>