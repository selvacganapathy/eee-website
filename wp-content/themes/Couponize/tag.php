<?php 
get_header();
?>
<div role="main">
            
    <div class="stripe-regular">
            
        <div class="row">                
                
            <div class="large-9 columns">
                                    
                <div class="coupons-wrapper">
                    <div class="row">
                        <div class="large-6 columns">
                            <h2>
                                <?php _e('Posts', 'Couponize');?> 
                                <span>- 
                                <?php 
                                    echo $wp_query->found_posts . ' ';
                                    _e('results', 'Couponize');
                                ?>
                                </span>
                            </h2>
                        </div>
                        <div class="large-6 columns">
                            <div class="ui-links sort-items">
                                <a href="<?php echo add_query_arg('sort', 'popular');?>" <?php if(isset($_GET['sort']) && $_GET['sort'] == 'popular') echo 'class="current"';?>><?php _e('Popular', 'Couponize');?></a> | 
                                <a href="<?php echo add_query_arg('sort', 'newest');?>" <?php if(!isset($_GET['sort']) || $_GET['sort'] == 'newest') echo 'class="current"';?>><?php _e('Newest', 'Couponize');?></a>
                            </div>
                        </div>
                    </div>
                    
                    <ul class="rr items-landscape">
                        <?php 
                        if(have_posts() ) : while(have_posts() ) : the_post();
                            $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                        ?>
                            <li class="wrapper-3">
                                <article <?php post_class('row');?>>
                                    <?php if($thumb != '') { ?>
                                        <div class="large-4 small-12 columns">
                                            <figure>
                                                <a href="<?php the_permalink();?>">
                                                    <img src="<?php echo aq_resize($thumb, 200, 200, true); ?>" alt="<?php the_title();?>">
                                                </a>
                                            </figure>
                                        </div>
                                    <?php } ?>
                                    <div class="large-<?php if($thumb != '') echo '8'; else echo '12';?> columns">
                                        <div class="col-middle">
                                            <h2 class="alt">
                                                <a href="<?php the_permalink();?>"><?php the_title();?></a>
                                            </h2>
                                            <p class="rr subtitle">
                                                <?php _e('Posted on', 'Couponize'); echo ' '; the_time("d/m/Y");?> 
                                                <?php _e('by', 'Couponize'); echo ' '; the_author_posts_link(); 
                                                echo ' | '; 
                                                comments_popup_link(esc_html__('0 comments','Couponize'), esc_html__('1 comment','Couponize'), '% '.esc_html__('comments','Couponize')); ?>
                                            </p>
                                            <div class="text secondary"><?php global $more; $more = 0; the_content('');?></div>
                                            <a class="input button red secondary responsive fixed blue-h" href="<?php the_permalink();?>"><?php _e('Read more', 'Couponize');?></a>
                                        </div>
                                    </div>
                                </article>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                        <?php get_template_part('includes/pagination');
                        endif; wp_reset_query();?>
                    <div style="clear: both"></div>
                </div>
                                    
            </div>
                
            <?php get_sidebar();?>
        </div>
    </div>
            
</div>
<?php get_footer();?>