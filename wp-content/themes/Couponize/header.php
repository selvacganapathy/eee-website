<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <title><?php wp_title('-');?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <!--[if (lt IE 9)&(!IEMobile)]>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() . 'css/foundation-ie8.css';?>">
    <![endif]-->    
    <?php
    global $teo_options;
    if(isset($teo_options['integration_header'])) echo $teo_options['integration_header'] . PHP_EOL; 
    wp_head(); 
    ?>
    <script>
        function loadCategoriesFromCountry(homeURL,countryCode, countryName,currentURL){
            jQuery("#countryNameValue").html(countryName);
            jQuery("#categoryList").show();
            console.log(countryCode);
            jQuery.ajax({
                url: homeURL+"/functions/retrieveCategories.php",
                data:{countryID:countryCode,currentURL:currentURL},
                success: function(result){
                    console.log(result);
                 jQuery("#categoryDetails").html(result);
             }});
        }
        function selectCategory(categoryID){
            jQuery("#categoryNameValue").html(categoryID);
            jQuery("#categoryID").val(categoryID);
        
        }
         function selectCountry(countryID){
             jQuery("#countryID").val(countryID);
        
        }
     </script>
     
<style>

</style>
</head>
<body <?php body_class();?>>
<!--[if lt IE 7]>
    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->
        
    <div class="page">

        <header role="banner" class="cf">
          
            <?php if(is_404() ) { ?>
                <div class="top-wrap">
                    <div class="row">
                        <div class="column">
                            <div class="logo text-center">
                                <a href="<?php echo home_url();?>"><?php bloginfo('name');?></a>
                            </div>
                        </div>
                    </div>
                </div>
            
                <h1 class="text-center alt"><?php _e('Page not found', 'Couponize');?></h1>
                <div class="row">
                    <div class="small-8 large-5 small-centered large-centered columns">
                        <p class="text text-center caption">
                            <?php _e('We\'re sorry, the page you are looking for does not exist or has been moved.', 'Couponize'); 
                            _e('Please check the link and try to search entry.', 'Couponize');?>
                        </p>
                    </div>
                </div>
            <?php } else { 
                if(isset($teo_options['enable_user_coupons']) && $teo_options['enable_user_coupons'] == 1) 
                    $guestpost = 1; 
                else
                    $guestpost = 0;
                ?>
                <div class="top-wrap">
                    <div class="row">
                        <div class="large-3 columns">
                            <div class="logo">
                                <a href="<?php echo home_url();?>">
                                    <?php 
                                    if(isset($teo_options['logo']) && $teo_options['logo'] != '') 
                                        echo '<img src="' . esc_url($teo_options['logo']) . '" />';
                                    elseif(isset($teo_options['logo_text']) && $teo_options['logo_text'] != '') 
                                        echo $teo_options['logo_text'];
                                    else 
                                        bloginfo('name');
                                    ?>
                                </a>
                            </div>
                        </div>
                        <nav class="large-<?php if($guestpost == 1) echo '6'; else echo '9';?> columns">
                            <?php wp_nav_menu(array(
                                'theme_location' => 'top-menu',
                                'container' => '',
                                'fallback_cb' => 'show_top_menu',
                                'menu_class' => 'rr main-menu',
                                'echo' => true
                                ) ); ?>
                        </nav>
                        <?php if($guestpost == 1) { ?>
                            <div class="large-3 columns">
                                <div class="account cf">
                                    <?php
                                    $url = '';
                                    if(isset($teo_options['frontend_submit']) && $teo_options['frontend_submit'] != '') {
                                        $url = get_permalink($teo_options['frontend_submit'][0]);
                                    }
                                    else {
                                        $url = site_url() . '/wp-admin/post-new.php?post_type=coupons';
                                    }
                                    ?>
                                    <a href="<?php echo $url;?>" class="input button blue tertiary icon plus"><?php _e('Add coupon', 'Couponize');?></a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>

                <?php if(is_home() || is_front_page() ) get_template_part('includes/featured'); 
            } 
            global $wpdb;
              $selectedCountryName ='Country';
                if (!empty($_REQUEST['countryID'])) {
                    $countryCodeQuery = "SELECT `country_name` from `countryCodes` where `country_code`='" . $_REQUEST['countryID'] . "' ";
                    $countryNameRes = $wpdb->get_results($countryCodeQuery);
                    $selectedCountryName = $countryNameRes[0]->country_name;
                }
                $selectedCategoryName = 'Category';
                if (!empty($_REQUEST['categoryID'])) {
                    $selectedCategoryName = $_REQUEST['categoryID'];
                }
                $selectedSearchName = '';
                if (!empty($_REQUEST['textDeal'])) {
                    $selectedSearchName = $_REQUEST['textDeal'];
                }
            ?>

            <div class="search-wrap stripe-white">
                <div class="row">
                    <form action="<?php echo home_url()."/coupons-page/";?>" method="GET">
                        <!--<input type="hidden" name="page_id" value="168">-->
                        <input type="hidden" name="countryID" id="countryID">
                        <input type="hidden" name="categoryID" id="categoryID">
                        
                        <nav class="small-3 large-2 columns">
                            <ul class="rr menu-browse">
                                <li class="input button primary">
                                    <p class="label" id="countryNameValue"><?php echo $selectedCountryName;?></p>
                                        <ul class="rr sub">
                                            <?php 
                                            $args = array();
                                            $args['hide_empty'] = 1;
                                            if(isset($teo_options['blog_category']) && $teo_options['blog_category'] != '')
                                                $args['exclude'] = implode(",", $teo_options['blog_category']);
                                             global $wpdb;
                                             $countryList = "SELECT distinct `countryCodes`.country_code, `countryCodes`.`country_name` from `countryCodes`, `deals` where `countryCodes`.`country_code` = `deals`.`country_code` ";
                                             $sthCountry = $wpdb->get_results($countryList);
                                             foreach($sthCountry as $countryVal) {?>
                                                  <li>
                                                      <a href="javascript:selectCountry('<?php echo $countryVal->country_code;?>');loadCategoriesFromCountry('<?php echo home_url();?>','<?php echo $countryVal->country_code;?>','<?php echo $countryVal->country_name;?>','<?php the_permalink();?>')">
                                                        <?php echo $countryVal->country_name;?>
                                                    </a>
                                                </li>
                              
                                            <?php } ?>
                                        </ul>
                                </li>
                            </ul>
                                
                        </nav>
                        <!--<div style="display: none;" id="categoryList">-->
                        <nav class="small-3 large-2 columns">
                            <ul class="rr menu-browse">
                                <li class="input button primary">
                                    <p class="label" id="categoryNameValue"><?php echo $selectedCategoryName;?></p>
                                        <ul class="rr sub" id="categoryDetails">
                                            <?php 
                                            $args = array();
                                            $args['hide_empty'] = 1;
                                            if(isset($teo_options['blog_category']) && $teo_options['blog_category'] != '')
                                                $args['exclude'] = implode(",", $teo_options['blog_category']);
                                             global $wpdb;
                                             $categoryList = "SELECT distinct `category` from `deals`; ";
                                             $sthCategory = $wpdb->get_results($categoryList);
                                             foreach($sthCategory as $categoryVal) {?>
                                                  <li>
                                                      <a href="javascript:selectCategory('<?php echo $categoryVal->category;?>');">
                                                      <?php echo $categoryVal->category;?>
                                                    </a>
                                                </li>
                              
                                            <?php } ?>
                                        </ul>
                                </li>
                            </ul>
                        </nav>
                        <div class="small-6 large-6 columns">
                            <label class="search-box">
                                <input type="text" name="textDeal" class="input field primary" placeholder="<?php _e('Search deals', 'Couponize');?>" value="<?php echo $selectedSearchName;?>">
                                <a href="#" class="icon clear ir" id="clear-sb"><?php echo _e('Clear','Couponize');?></a>
                            </label>
                        </div>
                        <!--</div>-->
                        <div class="small-3 large-2 columns">
                            <input type="submit" class="input button primary red" name="submit" value="<?php _e('Search', 'Couponize');?>">
                        </div>
                    </form>              
                </div>
            </div>
            
        </header>
