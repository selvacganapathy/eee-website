<div class="paginate">
	<?php
	if(function_exists('wp_pagenavi')) 
		wp_pagenavi(); 
	else { ?>
		<div class="pagination">
			<ul>
				<li class="left"><?php next_posts_link(esc_attr__('&laquo; Older Coupons', 'Couponize')); ?></li>
				<li class="right"><?php previous_posts_link(esc_attr__('Newer Coupons &raquo;', 'Couponize')); ?></li>
			</ul>
		</div>
	<?php } ?>
</div>
<div class="clear"></div>