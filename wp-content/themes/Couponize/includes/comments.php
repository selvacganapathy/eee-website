<?php if ( ! function_exists( 'couponize_comment' ) ) :
function couponize_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
   <article id="comment-<?php comment_ID(); ?>" class="top-comment">
        <div class="row">
            <div class="large-2 small-2 columns">
                <div class="avatar default">
                    <?php echo get_avatar($comment, 58); ?>
                </div>
            </div>
            <div class="large-10 columns">
                <p class="rr author">
                    <span class="name"><?php comment_author_link();?></span> <?php _e('says', 'Couponize');?>:
                </p>
                <div class="text-content">
                    <p><?php comment_text(); ?></p>
                    <?php if ($comment->comment_approved == '0') : ?>
                        <p>
                            <em class="moderation"><?php esc_html_e('Your comment is awaiting moderation.','Couponize') ?></em>
                        </p>
                        <br />
                    <?php endif; ?>
                </div>
                <?php $reply_link = get_comment_reply_link( array_merge( $args, array('reply_text' => esc_attr__('Reply','Couponize'),'depth' => $depth, 'max_depth' => $args['max_depth'])) ); ?>
                <div class="row">
                    <div class="large-6 columns">
                        <a href="#" class="posting-time"><?php echo(get_comment_date('F jS, Y G:i')) ?></a>
                    </div>
                    <?php if($reply_link) { ?>
                        <div class="large-6 columns ">
                            <?php 
                            echo $reply_link;
                            edit_comment_link( __( 'Edit', 'Couponize' ), ' ' );
                            ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </article>
<?php }
endif; ?>