<div class="stripe-regular items-carousel-wrap">
    <div class="row">
        <div class="column">
            <h2><?php _e('Hot Coupons', 'Couponize');?></h2>
        </div>
    </div>
    <div class="row collapse">
        <div class="column">
            <div class="items-carousel flexslider">
                <ul class="rr slides">
                    <?php 
                    global $db_pdo;
                    $sth = $db_pdo->prepare("select * from `deals` where `discount_percent` BETWEEN 85 and 90 limit 20");
                    $sth->execute();
                    $result = $sth->fetchAll();
                    foreach ($result as $row) {
                        $discount = $row['discount_percent'];
                        $date= strtotime($row['expiration_date']);
                        $code=$row['deal_id'];
                        $url = $row['deal_url'];   
                        $categories = $row['category'];
                        $category_id = $row['category_id'];
                        $thumb = $row['show_image_url'];
                        $title=$row['deal_title'];
                        $shortTitle=$row['deal_name'];
                        $price=$row['price']/100;
                        $currency=$row['currency'];
                                                
                        $fmt = numfmt_create( 'de_DE', NumberFormatter::CURRENCY );
						$price_formatted = numfmt_format_currency($fmt, $price,$currency);

                        $daysleft = round( ($date-time()) / 24 / 60 / 60);
                        $dealDetailsUrl=home_url("/")."?coupons=coupon-details&dealerID=$code";
                        ?>
                        <li>
                            <div class="wrapper-3 item-thumb">
                                <div class="top">
                                    <figure>
                                        <a href="<?php echo get_category_link( $category_id );?>">
                                            <img src="<?php echo $thumb ?>" width="200" height="200" alt="<?php echo $title;?>">
                                        </a>
                                    </figure>
                                    <h3 class="alt"><a href="<?php echo $dealDetailsUrl;?>"><?php echo substr($shortTitle, 0, 37);?></a></h3>
                                </div>
                                <div class="bottom">
                                    <?php if($discount != '') { ?>
                                        <p class="value secondary"><?php echo $discount."%";?></p>
                                    <?php } ?>
                                        <h6>
                                            <?php 
                                            if($date == '') 
                                                _e('VALID', 'Couponize');
                                            else if($daysleft <= 0) 
                                                _e('EXPIRED', 'Couponize'); 
                                            else echo sprintf( _n('%d day left.', '%d days left.', $daysleft, 'Couponize'), $daysleft );
                                            ?>
                                        </h6>
                                        <h6 class="value secondary"><?php echo $price_formatted;?></h6>
                                    <a href="<?php echo $dealDetailsUrl;?>" class="input button red secondary"><?php _e('Learn more', 'Couponize');?></a>
                                </div>
                            </div>
                        </li> 
                    <?php };?>
                </ul>
            </div>
        </div>
    </div>
</div>