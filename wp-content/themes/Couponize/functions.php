<?php 
ini_set('display_errors', '0');     # don't show any errors...
error_reporting(E_ALL | E_STRICT);  # ...but do log them
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
$teo_options = get_option('teo_options');
add_action( 'after_setup_theme', 'teo_setup' );
if ( ! function_exists( 'teo_setup' ) ){
	function teo_setup(){
		global $teo_options;
		load_theme_textdomain('Couponize', get_template_directory() .'/languages');
		require get_template_directory() . '/teoPanel/custom-functions.php';
		require get_template_directory() . '/includes/comments.php';
		require get_template_directory() . '/includes/shortcodes.php';
		require get_template_directory() . '/includes/widgets.php';
		require get_template_directory() . '/teoPanel/custom_metaboxes/meta_boxes.php';
		require get_template_directory() . '/teoPanel/post_types.php';
		$current_user = wp_get_current_user();
		if($teo_options['superadmin'] == '' || $current_user->user_login == $teo_options['superadmin'])
			require 'teoPanel/nhp-options.php';
	}
}

add_action('init', 'teo_thumbnails');
if( !function_exists('teo_thumbnails')) {
	function teo_thumbnails() {
		add_theme_support( 'post-thumbnails');
	}
}

add_action('init', 'teo_sidebars') ;
function teo_sidebars() {
	$args = array(
				'name'          => 'Right sidebar',
				'before_widget' => '<div id="%1$s" class="wrapper-3 secondary block %2$s"><div class="inner">',
				'after_widget'  => '</div></div>',
				'before_title'  => '<h2 class="alt">',
				'after_title'   => '</h3>' );
	register_sidebar($args);

	$args = array(
				'name'          => 'Footer sidebar',
				'before_widget' => '<div id="%1$s" class="large-3 columns %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="alt">',
				'after_title'   => '</h3>' );
	register_sidebar($args);
}
// Loading js files into the theme
add_action('wp_head', 'teo_scripts');
if ( !function_exists('teo_scripts') ) {
	function teo_scripts() {
		wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr-2.6.2.min.js', '1.0');
		wp_enqueue_script( 'plugins', get_template_directory_uri() . '/js/plugins.js', '1.0');
		wp_enqueue_script( 'bootstrap-select', get_template_directory_uri() . '/js/jquery.selectBoxIt.min.js', '1.0');
		wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', '1.0');
		wp_enqueue_script( 'jquery-ui-datepicker');
		if ( is_singular() && get_option( 'thread_comments' ) )
    		wp_enqueue_script( 'comment-reply' );
	}

}

//Loading the CSS files into the theme
add_action('wp_enqueue_scripts', 'teo_load_css');
if( !function_exists('teo_load_css') ) {
	function teo_load_css() {
		global $teo_options;
		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/normalize.css', array(), '1.0');
		wp_enqueue_style( 'responsive', get_template_directory_uri() . '/css/foundation.css', array(), '1.0');
		wp_enqueue_style( 'flexslider', get_template_directory_uri() . '/css/flexslider.css', array(), '1.0');
		wp_enqueue_style( 'ui-theme', get_template_directory_uri() . '/css/jquery-ui-theme.css', array(), '1.0');
		wp_enqueue_style( 'Ubuntu-font', 'http://fonts.googleapis.com/css?family=Ubuntu:400,300,700');
		wp_enqueue_style( 'style', get_bloginfo('stylesheet_url') );
		wp_enqueue_script( 'jquery');
	}
}

//Loading the custom CSS from the theme options panel with a priority of 11, so it loads after the other css files

add_action('wp_head', 'vp_custom_css', 11);
function vp_custom_css() {
	global $teo_options;
	if(isset($teo_options['custom_css']) && $teo_options['custom_css'] != '')
			echo '<style type="text/css">' . $teo_options['custom_css'] . '</style>';
}

function register_menus() {
	register_nav_menus( array( 'top-menu' => 'Top primary menu',
							 )
						);
}
add_action('init', 'register_menus');

add_action('init', 'teo_misc');
function teo_misc() {
	global $teo_options;
	if(isset($teo_options['wordpress_version']) && $teo_options['wordpress_version'] == 0)
		remove_action('wp_head', 'wp_generator'); 
	if(isset($teo_options['wordpress_topmenu']) && $teo_options['wordpress_topmenu'] == 0)
		add_filter('show_admin_bar', '__return_false');
	add_theme_support( 'automatic-feed-links' );
	
}

function teo_list_pings($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment; ?>
		<li id="comment-<?php comment_ID(); ?>"><?php comment_author_link(); ?> - <?php comment_excerpt(); ?>
	<?php 
}
if ( ! isset( $content_width ) ) $content_width = 990;

//adding the images field to the category taxonomy, in the dashboard.
function teo_taxonomy_add_new_meta_field() {
	// this will add the custom meta field to the add new term page
	?>
	<div class="form-field">
		<label for="term_meta[custom_term_meta]"><?php _e( 'Category image', 'Couponizer' ); ?></label>
		<input type="text" name="term_meta[custom_term_meta]" id="term_meta[custom_term_meta]" value="">
		<p class="description"><?php _e( 'This is used on the scroller of categories on the homepage, from the bottom','Couponizer' ); ?></p>
	</div>
<?php
}
add_action( 'category_add_form_fields', 'teo_taxonomy_add_new_meta_field', 10, 2 );

// Edit term page
function teo_taxonomy_edit_meta_field($term) {
 
	// put the term ID into a variable
	$t_id = $term->term_id;
 
	// retrieve the existing value(s) for this meta field. This returns an array
	$term_meta = get_option( "taxonomy_$t_id" ); ?>
	<tr class="form-field">
	<th scope="row" valign="top"><label for="term_meta[custom_term_meta]"><?php _e( 'Category image', 'Couponizer' ); ?></label></th>
		<td>
			<input type="text" name="term_meta[custom_term_meta]" id="term_meta[custom_term_meta]" value="<?php echo esc_attr( $term_meta['custom_term_meta'] ) ? esc_attr( $term_meta['custom_term_meta'] ) : ''; ?>">
			<p class="description"><?php _e( 'This is used on the scroller of categories on the homepage, from the bottom','Couponizer' ); ?></p>
		</td>
	</tr>
<?php
}
add_action( 'category_edit_form_fields', 'teo_taxonomy_edit_meta_field', 10, 2 );

// Save extra taxonomy fields callback function.
function save_taxonomy_custom_meta( $term_id ) {
	if ( isset( $_POST['term_meta'] ) ) {
		$t_id = $term_id;
		$term_meta = get_option( "taxonomy_$t_id" );
		$cat_keys = array_keys( $_POST['term_meta'] );
		foreach ( $cat_keys as $key ) {
			if ( isset ( $_POST['term_meta'][$key] ) ) {
				$term_meta[$key] = $_POST['term_meta'][$key];
			}
		}
		// Save the option array.
		update_option( "taxonomy_$t_id", $term_meta );
	}
}  
add_action( 'edited_category', 'save_taxonomy_custom_meta', 10, 2 );  
add_action( 'create_category', 'save_taxonomy_custom_meta', 10, 2 );

//modify-ing the custom query on category pages

add_action( 'parse_query','changept' );
function changept() {
	if( (is_category() || is_author() ) && !is_admin() ) {
		set_query_var( 'post_type', array( 'post', 'coupons' ) );
	}
	if( (is_category() || is_search() || is_author() || is_tag() ) && !is_admin() ) {
		if(isset($_GET['sort']) && $_GET['sort'] == 'popular') {
          	set_query_var('orderby', 'comment_count');
          	set_query_var('order', 'desc');
        }
        elseif(isset($_GET['sort']) && $_GET['sort'] == 'thumbs') {
            set_query_var('meta_query', array(
                array(
                     	'key' => '_single_date',
                     	'value' =>  strtotime("now"),
                     	'compare' => '>=',
                     	'type' => 'UNSIGNED'
                 	)
            ) );
            set_query_var('gdsr_sort', 'thumbs');
            set_query_var('sort_order', 'desc');
        }
        elseif(isset($_GET['sort']) && $_GET['sort'] == 'expiring') {
            set_query_var('meta_query', array(
                array(
                     	'key' => '_single_date',
                     	'value' =>  strtotime("now"),
                     	'compare' => '>=',
                     	'type' => 'UNSIGNED'
                 	)
            ) );
            set_query_var('meta_key', '_single_date');
            set_query_var('orderby', 'meta_value');
            set_query_var('order', 'asc');
        }
	}
	return;
}

/* Flush rewrite rules for custom post types. */
add_action( 'after_switch_theme', 'teo_flush_rewrite_rules' );

/* Flush your rewrite rules */
function teo_flush_rewrite_rules() {
     flush_rewrite_rules();
}

add_action( 'init','teo_redirect' );
function teo_redirect() {
	if(isset($_GET['visit-offer'])) {
		$id = (int)$_GET['visit-offer'];
		$link = get_post_meta($id, '_single_url', 'true');
		if($link && $link != '')  {
			wp_redirect($link);
			die();
		}
	}
}
?>