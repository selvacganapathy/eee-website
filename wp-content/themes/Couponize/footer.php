<?php
    function HotDeals() {
        return array("Hot Deal 1", "Hot Deal 2", "Hot Deal 3", "Hot Deal 4", "Hot Deal 5", "Hot Deal 6", "Hot Deal 7", "Hot Deal 8", "Hot Deal 9", "Hot Deal 10");
    }

    function PopularDeals() {
        return array("Popular Deal 1", "Popular Deal 2", "Popular Deal 3", "Popular Deal 4", "Popular Deal 5", "Popular Deal 6", "Popular Deal 7", "Popular Deal 8", "Popular Deal 9", "Popular Deal 10");
    }

    function PopularCompanies() {
        return array("Popular Company 1", "Popular Company 2", "Popular Company 3", "Popular Company 4", "Popular Company 5", "Popular Company 6", "Popular Company 7", "Popular Company 8", "Popular Company 9", "Popular Company 10");
    }
?>

<?php global $teo_options;?>
<footer role="contentinfo">
            
    <?php if(is_plugin_active('wysija-newsletters/index.php') && isset($teo_options['wisyja_id']) && $teo_options['wisyja_id'] != '') { ?>
        <div class="newsletter-wrap stripe-white">
            <div class="row">
                <div class="large-3 columns">
                    <h3><?php _e('Newsletter Signup', 'Couponize');?></h3>
                </div>
                <?php echo do_shortcode('[wysija_form id="' . $teo_options['wisyja_id'] . '"]');?>
            </div>
        </div>
    <?php } ?>
            
    <?php if(isset($teo_options['enable_popular_companies']) && $teo_options['enable_popular_companies'] == 1) { ?>
        <div class="stripe-regular">
            <!--
<div class="row">
                <div class="column">                  
                    <h2><?php _e('Hot Deals', 'Couponize');?></h2>
                </div>
            </div>
            <div class="row collapse">
                <div class="column">
                    <div class="popular-companies flexslider">
                        <ul class="rr slides">categories
                            <?php 
                            foreach(HotDeals() as $category) {
                            ?>
                                <li>
                                    <a href="#" class="wrapper-5 small">
                                        <img src="http://lorempixel.com/100/100" alt="<?php echo $category;?> coupons" title="<?php echo $category;?> coupons">
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
-->
            <!--
<div class="row">
                <div class="column">                  
                    <h2><?php _e('Popular Deals', 'Couponize');?></h2>
                </div>
            </div>
-->
            <!--
<div class="row collapse">
                <div class="column">
                    <div class="popular-companies flexslider">
                        <ul class="rr slides">categories
                            <?php 
                            foreach(PopularDeals() as $category) {
                            ?>
                                <li>
                                    <a href="#" class="wrapper-5 small">
                                        <img src="http://lorempixel.com/100/100" alt="<?php echo $category;?> coupons" title="<?php echo $category;?> coupons">
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">                  
                    <h2><?php _e('Popular Companies', 'Couponize');?></h2>
                </div>
            </div>
-->
            <!--
<div class="row collapse">
                <div class="column">
                    <div class="popular-companies flexslider">
                        <ul class="rr slides">categories
                            <?php 
                            foreach(PopularCompanies() as $category) {
                            ?>
                                <li>
                                    <a href="#" class="wrapper-5 small">
                                        <img src="http://lorempixel.com/100/100" alt="<?php echo $category;?> coupons" title="<?php echo $category;?> coupons">
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
-->
    <?php } ?>
            
   <!--
 <div class="stripe-darsk pre-footer-wrap">
        <div class="row">
            <?php dynamic_sidebar("Footer sidebar");?> 
        </div>
    </div>
            
    <div class="stripe-darker footer-wrap">
        <div class="row">
            <div class="large-3 columns">
                <div class="logo">
                    <a href="<?php echo home_url();?>">
                        <?php 
                            if(isset($teo_options['logo']) && $teo_options['logo'] != '') 
                                echo '<img src="' . esc_url($teo_options['logo']) . '" />';
                            elseif(isset($teo_options['logo_text']) && $teo_options['logo_text'] != '') 
                                echo $teo_options['logo_text'];
                            else 
                                bloginfo('name');
                            ?>
                        </a>
                </div>
            </div>
-->
<!--
            <div class="large-6 columns">
                <div class="copyright"><?php _e('Copyright', 'Couponize');?>  &copy; <?php echo date("Y "); bloginfo('name');?>, <?php _e('developed by', 'Couponize');?> <a href="http://teothemes.com"> TeoThemes</a>. <?php _e('All rights reserved', 'Couponize');?>.</div>
            </div>
            <div class="large-3 columns">
                <ul class="rr social">
                    <?php if(isset($teo_options['twitter_url']) && $teo_options['twitter_url'] != '') { ?>
                        <li><a href="<?php echo esc_url($teo_options['twitter_url']);?>" class="ir tw">Twitter</a></li>
                    <?php } ?>

                    <?php if(isset($teo_options['linkedin_url']) && $teo_options['linkedin_url'] != '') { ?>
                        <li><a href="<?php echo esc_url($teo_options['linkedin_url']);?>" class="ir in">LinkedIn</a></li>
                    <?php } ?>

                    <?php if(isset($teo_options['facebook_url']) && $teo_options['facebook_url'] != '') { ?>
                        <li><a href="<?php echo esc_url($teo_options['facebook_url']);?>" class="ir fb">Facebook</a></li>
                    <?php } ?>
                </ul>
            </div>
-->
        </div>
    </div>
            
</footer> 
          
<?php
if(isset($teo_options['integration_footer'])) echo $teo_options['integration_footer'] . PHP_EOL; 
    wp_footer(); 
?>
    </body>
</html>
