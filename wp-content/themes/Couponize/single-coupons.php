<?php 
get_header();
the_post();
$dealerID=$_GET['dealerID'];
include_once( ABSPATH . 'functions/db.connect.inc' );
global $db_pdo;
$dealsQuery = "SELECT * FROM deals  WHERE deal_id='$dealerID' ";
$sth = $db_pdo->prepare($dealsQuery);
$sth->execute();
$result = $sth->fetchAll();
foreach($result as $row) {
    $discount = $row['discount_percent'];
    $date= strtotime($row['expiration_date']);
    $code=$row['deal_id'];
    $offerlink = $row['deal_url'];   
    $category = $row['category'];
    $category_id = $row['category_id'];
    $thumb = $row['show_image_url'];
    $title=$row['deal_title'];
    $shortTitle=$row['deal_name'];
    $price=($row['price']/100);
    $currency=$row['currency'];
    
    $fmt = numfmt_create( 'de_DE', NumberFormatter::CURRENCY );
	$price_formatted = numfmt_format_currency($fmt, $price,$currency);

    $daysleft = round( ($date-time()) / 24 / 60 / 60);
    $postTime=date("d/m/Y",strtotime($row['post_date']));
    $decription=$row['description'];
//$discount = get_post_meta($post->ID, '_single_discount', true);
//$date = get_post_meta($post->ID, '_single_date', true);
//$code = get_post_meta($post->ID, '_single_code', true);
//$url = get_post_meta($post->ID, '_single_url', true);   
//$daysleft = round( ($date-time()) / 24 / 60 / 60);  
}
?>
<div role="main">     
    <div class="stripe-regular">                
        <div class="row">                
                
            <div class="large-9 columns">
                                    
                <article class="blog-post-content">
                    <section class="article-main-content">
                        <div class="wrapper-3 primary">
                            <div class="row">
                                <div class="column">
                                    <heading>
                                        <?php 
                                        if($thumb != '') { ?>
                                            <figure>
                                                <img src="<?php echo $thumb; ?>" width="650" height="260" alt="<?php echo $title;?>">
                                            </figure>
                                        <?php } ?>
                                        <h2 class="alt"><?php echo $title;?></h2>
                                        <p class="rr subtitle">
                                            <?php _e('Posted on', 'Couponize'); echo  $postTime;?> 
                                        </p>
                                    </heading>

                                    <div class="text-content">
                                        <div class="discount">
                                            <p class="value secondary"><?php echo $discount."%";?></p>
                                            <span>_______________</span>
                                            <h4>
                                                <?php 
                                                if($date == '') 
                                                    _e('VALID', 'Couponize');
                                                else if($daysleft <= 0) 
                                                    _e('EXPIRED', 'Couponize'); 
                                                else 
                                                    echo sprintf( _n('%d day left.', '%d days left.', $daysleft, 'Couponize'), $daysleft ); ?>
                                            </h4>
                                            <span>_______________</span>
                                            <h6 class="value secondary"><?php echo $price_formatted;?></h6>
                                            <p></p>
                                                <a target="_blank" href="<?php echo $offerlink; ?>" class="input button red secondary responsive">
                                                    <?php _e('Visit offer', 'Couponize');?>
                                                </a>
                                        </div>

                                        <div class="discount2">
                                            <?php echo $decription;?>
                                        </div>

                                        <div style="clear: both"></div>
                                    </div>

                                    <a href="#" class="input button dark secondary responsive fixed share-button right">Share</a>

                                    <!-- AddThis Button BEGIN -->
                                    <div class="share_toggle addthis_toolbox addthis_default_style">
                                        <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                        <a class="addthis_button_tweet"></a>
                                        <a class="addthis_button_pinterest_pinit"></a>
                                        <a class="addthis_counter addthis_pill_style"></a>
                                    </div>
                                    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5133cbfc3c9054b8"></script>
                                    <!-- AddThis Button END -->

                                </div>
                            </div>
                        </div>

                    </section>

                    <div class="single-related flexslider">
                        <h2>Related posts</h2>
                        <ul class="rr slides">
                            <?php 
                            $args = array();
                            $cats = array();
                            $categories = get_the_category();
                            foreach($categories as $cat) 
                                $cats[] = $cat->term_id;
                            $args['category__in'] = $cats;
                            $args['post_type'] = 'coupons';
                            $args['posts_per_page'] = 15;
                           //include_once( ABSPATH . 'functions/db.connect.inc' );
                              // global $db_pdo;
                               $sqlString = "SELECT * FROM deals ";
                               $condString = " WHERE deal_id <>0";
                               if (!empty($category)) {
                                   $condString.=" AND category='$category'";
                               }
                               echo "here";
                               $dealsQuery = $sqlString . $condString."LIMIT 0,15";
                               $sth = $db_pdo->query($dealsQuery);
                               if ($sth->rowCount() > 0){
                                foreach($sth as $row) {
                                    $discount = $row['discount_percent'];
                                    $date= strtotime($row['expiration_date']);
                                    $code=$row['deal_id'];
                                    $url = $row['deal_url'];   
                                    $category = $row['category'];
                                    $category_id = $row['category_id'];
                                    $thumb = $row['show_image_url'];
                                    $title=$row['deal_title'];
                                    $shortTitle=$row['deal_name'];
                                    $price=$row['price'];
                                    $currency=$row['currency'];
                                    
                                    $fmt = numfmt_create( 'de_DE', NumberFormatter::CURRENCY );
									$price_formatted = numfmt_format_currency($fmt, $price,$currency);

                                    $daysleft = round( ($date-time()) / 24 / 60 / 60);
                                    $dealDetailsUrl=home_url("/")."?coupons=coupon-details&dealerID=$code";
                                ?>
                                <li>
                                    <div class="wrapper-3 item-thumb">
                                        <div class="top">
                                            <figure>
                                                <a href="<?php echo get_category_link( $category_id );?>">
                                                    <img src="<?php echo $thumb; ?>"  width="200" height="110" alt="<?php the_title();?>">
                                                </a>
                                            </figure>
                                            <h2 class="alt"><a href="<?php echo $dealDetailsUrl;?>"><?php echo substr($shortTitle, 0, 30);?></a></h2>
                                        </div>
                                        <div class="bottom">
                                            <?php if($discount != '') { ?>
                                                <p class="value secondary"><?php echo $discount."%";?></p>
                                            <?php } ?>
                                                <h6 class="value secondary"><?php echo $price_formatted;?></h6>
                                                <h6><?php if($daysleft <= 0) _e('EXPIRED', 'Couponize'); else echo sprintf( _n('%d day left.', '%d days left.', $daysleft, 'Couponize'), $daysleft ); ?></h6>
                                            <a href="<?php echo $dealDetailsUrl;?>" class="input button red secondary"><?php _e('Learn more', 'Couponize');?></a>
                                        </div>
                                    </div>
                                </li> 
                               <?php }};?>
                        </ul>
                    </div>

                    
                  </article>
                  
            </div>
            
        </div>
    </div>
            
</div>
<?php get_footer();?>