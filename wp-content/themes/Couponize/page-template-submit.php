<?php
/*
Template name: Front-end submit coupon 
*/ 
if(isset($_POST['submit-coupon']) ) {
    require get_template_directory() . '/includes/mediaUpload.php';
    $errors = array();
    if($_POST['coupon_title'] == '') {
        $errors[] = __("The title can't be empty.", "Couponize");
    }
    if($_POST['coupon_cat'] == '') {
        $errors[] = __("The category can't be empty.", "Couponize");
    }
    if($_POST['coupon_discount'] == '') {
        $errors[] = __("The discount field can't be empty.", "Couponize");
    }
    if($_FILES['coupon_image']['error'] != 0) {
        $errors[] = __('You need to upload an image for the coupon!', 'Teo');
    }
    if(count($errors) == 0) {
        $allowed_tags = wp_kses_allowed_html( 'post' );
        $title = esc_attr($_POST['coupon_title']);
        $description = wp_kses($_POST['coupon_description'], $allowed_tags);
        $cat = (int)$_POST['coupon_cat'];
        $discount = esc_attr($_POST['coupon_discount']);
        $code = esc_attr($_POST['coupon_code']);
        $url = esc_url($_POST['coupon_url']);
        $date = $_POST['coupon_date'];
        if(is_user_logged_in() ) { 
            $current_user = wp_get_current_user();
            $user_id = $current_user->ID;
        }
        else {
            $user_id = 1;
        }

        $post = array(
          'comment_status' => 'open',
          'ping_status'    => 'open',
          'post_author'    => $user_id,
          'post_content'   => $description,
          'post_status'    => 'pending',
          'post_title'     => $title,
          'post_type'      => 'coupons',
        );  
        $post_id = wp_insert_post($post);
        if($post_id == 0) {
            $errors[] = __("The listing couldn't be added to the database, try again.", "Teo");
        }
        else {
            $cat = (array)$cat;
            wp_set_post_terms($post_id, $cat, 'category'); //setting the correct category
            $tmp = new MediaUpload;
            if($_FILES['coupon_image']['error'] == 0) {
                $featured_image = $tmp->saveUpload( 'coupon_image' );
                add_post_meta($post_id, '_thumbnail_id', $featured_image['attachment_id']);
            }

            //setting custom fields info
            if($discount != '') {
                add_post_meta($post_id, '_single_discount', $discount);
            }

            if($code != '') {
                add_post_meta($post_id, '_single_code', $code);
            }

            if($date != '') {
                add_post_meta($post_id, '_single_date', strtotime($date) );
            }

            if($url != '') {
                add_post_meta($post_id, '_single_url', $url);
            }
            $ok=1;
        }
    }
}
get_header();
the_post();
?>
<div role="main">     
    <div class="stripe-regular">                
        <div class="row">                
                
            <div class="large-9 columns">
                                    
                <article class="blog-post-content">
                    <section class="article-main-content">
                        <div class="wrapper-3 primary">
                            <div class="row">
                                <div class="column">
                                    <heading>
                                        <h2 class="alt"><?php the_title();?></h2>
                                    </heading>

                                    <div class="text-content frontend-submit">
                                         <?php if(isset($errors) && count($errors) > 0) { ?>
                                            <div class="large-12 column alert alert-danger">
                                                 <?php foreach($errors as $error) echo $error . '<br />'; ?>
                                            </div>
                                        <?php } ?>
                                        
                                        <?php if(isset($ok) ) { ?>
                                            <div class="large-12 column alert alert-success">
                                                <?php _e('Coupon added successfully, it must be approved by the administrator now!', 'Couponize');?>
                                            </div>
                                        <?php } ?>

                                        <form enctype="multipart/form-data" action="<?php echo(get_permalink($post->ID)); ?>" method="post">

                                            <div class="large-6 column">
                                                <label for="title"><?php _e('Coupon title', 'Couponize');?></label>
                                                <input id="title" class="input field primary" name="coupon_title" />
                                            </div>
                                            <div class="large-6 column">
                                                <label for="category"><?php _e('Coupon category', 'Couponize');?></label>
                                                <select class="selectpicker btn-block" name="coupon_cat" id="category">
                                                    <?php
                                                    $categories = get_terms('category');
                                                    foreach($categories as $category) { ?>
                                                        <option value="<?php echo $category->term_id;?>"><?php echo $category->name;?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>

                                            <div class="cf"></div><br />

                                            <div class="large-12 column">
                                                <label for="description"><?php _e('Coupon description', 'Couponize');?></label>
                                                <textarea rows="5" id="description" class="input field" name="coupon_description"></textarea>
                                            </div>

                                            <div class="cf"></div><br />

                                            <div class="large-6 column">
                                                <label for="discount_value"><?php _e('Discount Value(e.g $30 OFF)', 'Couponize');?></label>
                                                <input id="discount_value" class="input field primary" name="coupon_discount" />
                                            </div>
                                            <div class="large-6 column">
                                                <label for="coupon_code"><?php _e('Coupon Code', 'Couponize');?></label>
                                                <input id="coupon_code" class="input field primary" name="coupon_code" />
                                            </div>

                                            <div class="cf"></div><br />

                                            <div class="large-6 column">
                                                <label for="coupon_url"><?php _e('Coupon URL(if applicable)', 'Couponize');?></label>
                                                <input id="coupon_url" class="input field primary" name="coupon_url" />
                                            </div>
                                            <div class="large-6 column">
                                                <label for="expiry_date"><?php _e('Expiry date', 'Couponize');?></label>
                                                <input id="expiry_date" class="input field primary dateexpiry" name="coupon_date" />
                                            </div>

                                            <div class="cf"></div> <br />

                                            <div class="large-12 column">
                                                <label for="image"><?php _e('Coupon thumbnail image:', 'Couponize');?></label>
                                                <input id="image" type="file" name="coupon_image" />
                                            </div>

                                            <div class="cf"></div> <br />

                                            <input class="input button primary red" type="submit" name="submit-coupon" value="Add coupon" />
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </section>
                    
                  </article>
                  
            </div>
                
            <?php get_sidebar();?> 
            
        </div>
    </div>
            
</div>
<?php get_footer();?>