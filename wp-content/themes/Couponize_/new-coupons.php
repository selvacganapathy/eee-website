<div class="stripe-regular items-carousel-wrap">
    <div class="row">
        <div class="column">
            <h2><?php _e('New Coupons', 'Couponize');?></h2>
        </div>
    </div>
    <div class="row collapse">
        <div class="column">
            <div class="items-carousel flexslider">
                <ul class="rr slides">
                    <?php 
                    $args = array();
                    $args['post_type'] = 'coupons';
                    $args['posts_per_page'] = 20;
                    $query = new WP_Query($args);
                    while($query->have_posts() ) : $query->the_post();
                        $discount = get_post_meta($post->ID, '_single_discount', true);
                        $date = get_post_meta($post->ID, '_single_date', true);
                        $code = get_post_meta($post->ID, '_single_code', true);
                        $url = get_post_meta($post->ID, '_single_url', true);                             
                        $thumbs = array();
                        $categories = get_the_category();
                        $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                        if($categories) { 
                            foreach($categories as $category) {
                                $image = get_option('taxonomy_' . $category->term_id);
                                if($image) {
                                    if($thumb == '') {
                                        $thumb = $image['custom_term_meta'];
                                    }
                                    $category_id = $category->term_id;
                                }
                            }
                        }
                        $daysleft = round( ($date-time()) / 24 / 60 / 60);
                        ?>
                        <li>
                            <div class="wrapper-3 item-thumb">
                                <div class="top">
                                    <figure>
                                        <a href="<?php echo get_category_link( $category_id );?>">
                                            <img src="<?php echo aq_resize($thumb, 200, 110, true); ?>" alt="<?php the_title();?>">
                                        </a>
                                    </figure>
                                    <h2 class="alt"><a href="<?php the_permalink();?>"><?php truncate_title(37);?></a></h2>
                                </div>
                                <div class="bottom">
                                    <?php if($discount != '') { ?>
                                        <p class="value secondary"><?php echo $discount;?></p>
                                    <?php } ?>
                                        <h6>
                                            <?php 
                                            if($date == '') 
                                                _e('VALID', 'Couponize');
                                            else if($daysleft <= 0) 
                                                _e('EXPIRED', 'Couponize'); 
                                            else echo sprintf( _n('%d day left.', '%d days left.', $daysleft, 'Couponize'), $daysleft ); ?>
                                        </h6>
                                    <a href="<?php the_permalink();?>" class="input button red secondary"><?php _e('Learn more', 'Couponize');?></a>
                                </div>
                            </div>
                        </li> 
                    <?php endwhile; wp_reset_postdata();?>
                </ul>
            </div>
        </div>
    </div>
</div>