jQuery(function() {

  /* wysija newsletter integration start */

  var firstinput = jQuery('.wysija-paragraph:nth-child(1)'),
      secondinput = jQuery('.wysija-paragraph:nth-child(2)'),
      submitbutton = jQuery('.wysija-submit'),
      label1text = firstinput.find('label').text(),
      label2text = secondinput.find('label').text();

  firstinput.addClass('large-3 columns');
  firstinput.find('input').attr("placeholder", label1text);
  secondinput.find('input').attr("placeholder", label2text);
  secondinput.addClass('large-4 columns');
  submitbutton.addClass('input large-2 columns');

  /* wysija newsletter integration end */

  jQuery('#contact_submit, #searchsubmit, .wpcf7-submit').addClass('input button dark secondary responsive');

  jQuery('.toggle_coupon').on('click', function(e) {
      var box = jQuery(this),
          href = box.attr('href');
      box.animate({"opacity" : "hide"}, 500, function() { 
        box.parent().find('.hidden_coupon').css("display", "block");
      });
      if(href == '#') {
          e.preventDefault();
      }
  });

  jQuery('.share-button').on('click', function() { 
    jQuery('.share_toggle').fadeToggle(300);
  });

  jQuery('a[href=#]').on('click', function(e) {
    e.preventDefault();
  });

  // main menu responsive
  jQuery('.main-menu').mobileMenu({
    defaultText: 'Navigate to...',
    className: 'select-main-menu'
  });

  jQuery('.select-main-menu').customSelect({
    customClass: 'input button dark secondary'
  });

  jQuery('.popular-companies-list').mobileMenu({
    defaultText: 'Select company...',
    className: 'select-popular-companies-list'
  });

  // clear searchbox button functionality
  jQuery('#clear-sb').on('click', function() {
    var field = jQuery(this).parent().children('input');
    field.val('');
    field.focus();
  });


  // main menu indicator
  // var temp;
  // jQuery('.main-menu li').hover(function() {
  //   temp = jQuery(this).parent().children('.current')
  //   temp.removeClass('current');
  //   jQuery(this).addClass('current');
  // }, function() {
  //   jQuery(this).removeClass('current');
  //   temp.addClass('current');
  // });


  // menu-browse open/close
  var temp2 = jQuery('.menu-browse .input');
  var temp3 = jQuery('.main-menu .submenu-wrap');
  jQuery('html').on('click', function() {
    temp2.children('.sub').hide();
    temp3.children('.submenu').hide();
    temp3.removeClass('opaque');
  });  
  temp2.on('click', function(event) {   
    event.stopPropagation(); 
    jQuery(this).children('.sub').toggle();
  });
  temp3.on('click', function(event) {   
    event.stopPropagation(); 
    jQuery(this).children('.submenu').toggle();
    if (temp3.children('.submenu').is(':visible')) {
      temp3.addClass('opaque');
    } else {
      temp3.removeClass('opaque');
    }
  });


  // main-slider
  jQuery('.main-slider').flexslider({
    animation: "slide"
  });

  // popular companies
  jQuery('.popular-companies').flexslider({
    animation: "slide",
    itemWidth: 134,
    itemMargin: 0,
    minItems: 2,
    maxItems: 7
  });

   // popular companies
  jQuery('.items-carousel').flexslider({
    animation: "slide",
    itemWidth: 240,
    itemMargin: 0,
    minItems: 1,
    maxItems: 4,
    slideshowSpeed: 4000
  });

  jQuery('.single-related').flexslider({
    animation: "slide",
    itemWidth: 175,
    itemMargin: -1,
    minItems: 1,
    maxItems: 3,
    slideshowSpeed: 4000
  });

  jQuery('.popular-carousel').flexslider({
    animation: "slide",
    itemWidth: 240,
    itemMargin: 0,
    minItems: 1,
    maxItems: 4,
    slideshowSpeed: 5500
  });

   // testimonials
  jQuery('.testimonials-box').flexslider({
    animation: "fade"
  });

  // checklist

  jQuery('.checklist input').parent().removeClass('checked');
  jQuery('.checklist input:checked').parent().addClass('checked');
  
  jQuery('.checklist input').on('change', function() {
    if (jQuery(this).is(':checked') && !jQuery(this).parent().hasClass('checked')) {
      jQuery(this).parent().addClass('checked');
    } else if (!jQuery(this).is(':checked') && jQuery(this).parent().hasClass('checked')) {
      jQuery(this).parent().removeClass('checked');
    }
  });

  jQuery('.selectpicker').selectBoxIt();
  jQuery('input.dateexpiry').datepicker();


  // days filter slider

  jQuery( '.filter-days .slider' ).slider({
    range: true,
    min: 0,
    max: 120,
    values: [ 0, 80 ],
    slide: function( event, ui ) {
      jQuery('.input-min').val( ui.values[0] );
      jQuery('.input-max').val( ui.values[1] );
      jQuery('.filter-days .info .begin').text( ui.values[0] + ( ui.values[0] != 1 ? ' days' : ' day' ) );
      jQuery('.filter-days .info .end').text( ui.values[1] + ( ui.values[1] != 1 ? ' days' : ' day' ) );
    }
  });

  var begin = jQuery('.filter-days .slider').slider('values', 0);
  var end = jQuery('.filter-days .slider').slider('values', 1);
  jQuery('.filter-days .info .begin').text( begin + ( begin != 1 ? ' days' : ' day' ) );
  jQuery('.filter-days .info .end').text( end + ( end != 1 ? ' days' : ' day' ) );


});
