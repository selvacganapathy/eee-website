<?php 
/*
Template name: Homepage template 
*/ 
global $teo_options;
get_header();?>
<div role="main">
    <?php 
    if(isset($teo_options['enable_new_coupons']) && $teo_options['enable_new_coupons'] == 1) 
        get_template_part('new-coupons');
    if(isset($teo_options['enable_popular_coupons']) && $teo_options['enable_popular_coupons'] == 1) 
        get_template_part('popular-coupons');
    
    if(isset($teo_options['homepage_page']) && $teo_options['homepage_page'] != '') {
        $query = new WP_Query('page_id=' . $teo_options['homepage_page'][0]);
        while($query->have_posts() ) : $query->the_post();
    ?>
      <div class="stripe-regular single_page">
          <div class="row">
              <?php the_content('');?>            
          </div>
      </div>
    <?php endwhile; wp_reset_postdata();
    }
get_footer(); ?>