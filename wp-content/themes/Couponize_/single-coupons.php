<?php 
get_header();
the_post();
$discount = get_post_meta($post->ID, '_single_discount', true);
$date = get_post_meta($post->ID, '_single_date', true);
$code = get_post_meta($post->ID, '_single_code', true);
$url = get_post_meta($post->ID, '_single_url', true);   
$daysleft = round( ($date-time()) / 24 / 60 / 60);        
?>
<div role="main">     
    <div class="stripe-regular">                
        <div class="row">                
                
            <div class="large-9 columns">
                                    
                <article class="blog-post-content">
                    <section class="article-main-content">
                        <div class="wrapper-3 primary">
                            <div class="row">
                                <div class="column">
                                    <heading>
                                        <?php $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                                        if($thumb != '') { ?>
                                            <figure>
                                                <img src="<?php echo aq_resize($thumb, 650, 260, true); ?>" alt="<?php the_title();?>">
                                            </figure>
                                        <?php } ?>
                                        <h2 class="alt"><?php the_title();?></h2>
                                        <p class="rr subtitle">
                                            <?php _e('Posted on', 'Couponize'); echo ' '; the_time("d/m/Y");?> 
                                            <?php _e('by', 'Couponize'); echo ' '; the_author_posts_link(); 
                                            echo ' | '; 
                                            comments_popup_link(esc_html__('0 comments','Couponize'), esc_html__('1 comment','Couponize'), '% '.esc_html__('comments','Couponize')); ?>
                                        </p>
                                    </heading>

                                    <div class="text-content">
                                        <div class="discount">
                                            <p class="value secondary"><?php echo $discount;?></p>
                                            <span>_______________</span>
                                            <h4>
                                                <?php 
                                                if($date == '') 
                                                    _e('VALID', 'Couponize');
                                                else if($daysleft <= 0) 
                                                    _e('EXPIRED', 'Couponize'); 
                                                else 
                                                    echo sprintf( _n('%d day left.', '%d days left.', $daysleft, 'Couponize'), $daysleft ); ?>
                                            </h4>
                                            <?php if($code != '') { 
                                                if($url != '') {
                                                    $button = __('Reveal offer', 'Couponize');
                                                    $offerlink = home_url('/') . '?visit-offer=' . $post->ID;
                                                }
                                                else {
                                                    $button = __('Show Coupon', 'Couponize');
                                                    $offerlink = '#';
                                                }
                                                ?> 
                                                <a <?php if($offerlink != '') echo 'target="_blank"';?> href="<?php echo $offerlink; ?>" class="input button red secondary responsive toggle_coupon">
                                                    <?php echo $button;?>
                                                </a>
                                                <span class="input button border high secondary responsive hidden_coupon">
                                                    <?php echo $code;?>
                                                </span>
                                            <?php } 
                                            elseif($url != '') {
                                                $offerlink = home_url('/') . '?visit-offer=' . $post->ID; ?>
                                                <a target="_blank" href="<?php echo $offerlink; ?>" class="input button blue secondary responsive">
                                                    <?php _e('Visit offer', 'Couponize');?>
                                                </a>
                                            <?php } ?>
                                        </div>

                                        <div class="discount2">
                                            <?php the_content('');?>
                                        </div>

                                        <div style="clear: both"></div>
                                    </div>

                                    <a href="#" class="input button dark secondary responsive fixed share-button right">Share</a>

                                    <!-- AddThis Button BEGIN -->
                                    <div class="share_toggle addthis_toolbox addthis_default_style">
                                        <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                        <a class="addthis_button_tweet"></a>
                                        <a class="addthis_button_pinterest_pinit"></a>
                                        <a class="addthis_counter addthis_pill_style"></a>
                                    </div>
                                    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5133cbfc3c9054b8"></script>
                                    <!-- AddThis Button END -->

                                </div>
                            </div>
                        </div>

                    </section>

                    <div class="single-related flexslider">
                        <h2>Related posts</h2>
                        <ul class="rr slides">
                            <?php 
                            $args = array();
                            $cats = array();
                            $categories = get_the_category();
                            foreach($categories as $cat) 
                                $cats[] = $cat->term_id;
                            $args['category__in'] = $cats;
                            $args['post_type'] = 'coupons';
                            $args['posts_per_page'] = 15;
                            $query = new WP_Query($args);
                            while($query->have_posts() ) : $query->the_post();
                                $discount = get_post_meta($post->ID, '_single_discount', true);
                                $date = get_post_meta($post->ID, '_single_date', true);
                                $code = get_post_meta($post->ID, '_single_code', true);
                                $url = get_post_meta($post->ID, '_single_url', true);                             
                                $thumbs = array();
                                $categories = get_the_category();
                                $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                                if($categories) { 
                                    foreach($categories as $category) {
                                        $image = get_option('taxonomy_' . $category->term_id);
                                        if($image) {
                                            if($thumb == '') {
                                                $thumb = $image['custom_term_meta'];
                                            }
                                            $category_id = $category->term_id;
                                        }
                                    }
                                }
                                $daysleft = round( ($date-time()) / 24 / 60 / 60);
                                ?>
                                <li>
                                    <div class="wrapper-3 item-thumb">
                                        <div class="top">
                                            <figure>
                                                <a href="<?php echo get_category_link( $category_id );?>">
                                                    <img src="<?php echo aq_resize($thumb, 200, 110, true); ?>" alt="<?php the_title();?>">
                                                </a>
                                            </figure>
                                            <h2 class="alt"><a href="<?php the_permalink();?>"><?php truncate_title(30);?></a></h2>
                                        </div>
                                        <div class="bottom">
                                            <?php if($discount != '') { ?>
                                                <p class="value secondary"><?php echo $discount;?></p>
                                            <?php } ?>
                                                <h6><?php if($daysleft <= 0) _e('EXPIRED', 'Couponize'); else echo sprintf( _n('%d day left.', '%d days left.', $daysleft, 'Couponize'), $daysleft ); ?></h6>
                                            <a href="<?php the_permalink();?>" class="input button red secondary"><?php _e('Learn more', 'Couponize');?></a>
                                        </div>
                                    </div>
                                </li> 
                            <?php endwhile; wp_reset_postdata();?>
                        </ul>
                    </div>

                    <?php comments_template('', true);?>
                    
                  </article>
                  
            </div>
                
            <?php get_sidebar();?> 
            
        </div>
    </div>
            
</div>
<?php get_footer();?>