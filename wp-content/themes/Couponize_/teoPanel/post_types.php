<?php 
add_action('init', 'teo_post_types');
function teo_post_types() {
  $labels = array(
    'name' => _x('Coupons', 'post type general name', 'Couponize'),
    'singular_name' => _x('Coupon', 'post type singular name', 'Couponize'),
    'add_new' => _x('Add Coupon', 'portfolio_item', 'Couponize'),
    'add_new_item' => __('Add a new coupon', 'Couponize'),
    'edit_item' => __('Edit coupon', 'Couponize'),
    'new_item' => __('New coupon', 'Couponize'),
    'all_items' => __('All coupons', 'Couponize'),
    'view_item' => __('View coupon details', 'Couponize'),
    'search_items' => __('Search coupons', 'Couponize'),
    'not_found' =>  __('No coupons found', 'Couponize'),
    'not_found_in_trash' => __('No coupons in the trash.' , 'Couponize'), 
    'parent_item_colon' => '',
    'menu_name' => 'Coupons'

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array('title', 'editor', 'author', 'thumbnail', 'comments')
  ); 
  
  register_post_type('coupons',$args);
  register_taxonomy_for_object_type( 'category', 'coupons' ); 
}
?>