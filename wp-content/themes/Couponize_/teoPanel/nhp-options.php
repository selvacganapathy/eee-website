<?php
/*
 * 
 * Require the framework class before doing anything else, so we can use the defined urls and dirs
 * Also if running on windows you may have url problems, which can be fixed by defining the framework url first
 *
 */
//define('NHP_OPTIONS_URL', site_url('path the options folder'));
if(!class_exists('NHP_Options')){
	require_once( dirname( __FILE__ ) . '/options/options.php' );
}

/*
 * 
 * Custom function for filtering the sections array given by theme, good for child themes to override or add to the sections.
 * Simply include this function in the child themes functions.php file.
 *
 * NOTE: the defined constansts for urls, and dir will NOT be available at this point in a child theme, so you must use
 * get_template_directory_uri() if you want to use any of the built in icons
 *
 */
function add_another_section($sections){
	
	//$sections = array();
	$sections[] = array(
				'title' => __('A Section added by hook', 'nhp-opts'),
				'desc' => __('<p class="description">This is a section created by adding a filter to the sections array, great to allow child themes, to add/remove sections from the options.</p>', 'nhp-opts'),
				//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
				//You dont have to though, leave it blank for default.
				'icon' => trailingslashit(get_template_directory_uri()).'options/img/glyphicons/glyphicons_062_attach.png',
				//Lets leave this as a blank section, no options just some intro text set above.
				'fields' => array()
				);
	
	return $sections;
	
}//function
//add_filter('nhp-opts-sections-twenty_eleven', 'add_another_section');


/*
 * 
 * Custom function for filtering the args array given by theme, good for child themes to override or add to the args array.
 *
 */
function change_framework_args($args){
	
	//$args['dev_mode'] = false;
	
	return $args;
	
}//function
//add_filter('nhp-opts-args-twenty_eleven', 'change_framework_args');









/*
 * This is the meat of creating the optons page
 *
 * Override some of the default values, uncomment the args and change the values
 * - no $args are required, but there there to be over ridden if needed.
 *
 *
 */

function setup_framework_options(){

	$args = array();

//Set it to dev mode to view the class settings/info in the form - default is false
$args['dev_mode'] = true;

//google api key MUST BE DEFINED IF YOU WANT TO USE GOOGLE WEBFONTS
//$args['google_api_key'] = '***';

//Remove the default stylesheet? make sure you enqueue another one all the page will look whack!
//$args['stylesheet_override'] = true;

//Add HTML before the form
//$args['intro_text'] = __('<p>Don\'t forget to save the settings!</p>', 'nhp-opts');

//Setup custom links in the footer for share icons
$args['share_icons']['facebook'] = array(
										'link' => 'http://www.facebook.com/finaldestiny16',
										'title' => 'My facebook account', 
										'img' => NHP_OPTIONS_URL.'img/glyphicons/glyphicons_320_facebook.png'
										);
$args['share_icons']['themeforest'] = array(
										'link' => 'http://themeforest.net/user/FinalDestiny',
										'title' => 'My themeforest account',
										'img' => NHP_OPTIONS_URL . 'img/glyphicons/glyphicons_050_link.png'
										);
//Choose to disable the import/export feature
//$args['show_import_export'] = false;

//Choose a custom option name for your theme options, the default is the theme name in lowercase with spaces replaced by underscores
$args['opt_name'] = 'teo_options';

$args['google_api_key'] = 'AIzaSyB0_zr4gsc6PkFl5UiHDj6ROiXtuYb7QBk';

//Custom menu icon
//$args['menu_icon'] = '';

//Custom menu title for options page - default is "Options"
$args['menu_title'] = __('Couponize Options', 'nhp-opts');

//Custom Page Title for options page - default is "Options"
$args['page_title'] = __('Couponize Theme Options', 'nhp-opts');

//Custom page slug for options page (wp-admin/themes.php?page=***) - default is "nhp_theme_options"
$args['page_slug'] = 'couponize_options';

//Custom page capability - default is set to "manage_options"
//$args['page_cap'] = 'manage_options';

//page type - "menu" (adds a top menu section) or "submenu" (adds a submenu) - default is set to "menu"
//$args['page_type'] = 'submenu';

//parent menu - default is set to "themes.php" (Appearance)
//the list of available parent menus is available here: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
//$args['page_parent'] = 'themes.php';

//custom page location - default 100 - must be unique or will override other items
$args['page_position'] = null;

$args['footer_credit'] = '';

//Custom page icon class (used to override the page icon next to heading)
//$args['page_icon'] = 'icon-themes';

//Want to disable the sections showing as a submenu in the admin? uncomment this line
//$args['allow_sub_menu'] = false;
		
//Set ANY custom page help tabs - displayed using the new help tab API, show in order of definition		
$args['help_tabs'][] = array(
							'id' => 'nhp-opts-1',
							'title' => __('Theme Information 1', 'nhp-opts'),
							'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'nhp-opts')
							);
$args['help_tabs'][] = array(
							'id' => 'nhp-opts-2',
							'title' => __('Theme Information 2', 'nhp-opts'),
							'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'nhp-opts')
							);

//Set the Help Sidebar for the options page - no sidebar by default										
$args['help_sidebar'] = __('<p>This is the sidebar content, HTML is allowed.</p>', 'nhp-opts');



$sections = array();

$sections[] = array(
				'title' => __('General Settings', 'nhp-opts'),
				'desc' => __('<p class="description">Here you can configure the general aspects of the theme.!</p>', 'nhp-opts'),
				//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
				//You dont have to though, leave it blank for default.
				'icon' => NHP_OPTIONS_URL.'img/glyphicons/glyphicons_062_attach.png',
				'fields' => array(
					array(
						'id' => 'logo_text',
						'type' => 'text',
						'title' => 'The text used on the logo',
						'sub_desc' => 'This is the logo text(the default one is the blog\'s name',
						'std' => get_bloginfo('name')
						),
					array(
						'id' => 'logo',
						'type' => 'upload',
						'title' => 'Logo',
						'sub_desc' => 'If you choose this option, the logo text will no longer be used. Recommended width / height: 220px / 30-35px'
						),
					array(
						'id' => 'favicon',
						'type' => 'upload',
						'title' => 'Favicon',
						'sub_desc' => 'This is the little icon in the address bar for your website'
						),
					array(
						'id' => 'bg_image',
						'type' => 'upload',
						'title' => 'Background Image',
						'sub_desc' => 'Use this if you want to override the default background image'
						),
					array(
						'id' => 'superadmin',
						'type' => 'text',
						'title' => 'Super admin username',
						'sub_desc' => '<strong>You can show the Theme options just for one admin user with this option! BE CAREFUL WITH THIS ONE!</strong>.',
						'std' => ''
						),
					array(
						'id' => 'wordpress_version',
						'type' => 'button_set',
						'options' => array('1' => 'Yes', '0' => 'No'),
						'title' => 'Show the wordpress version in your source code?',
						'std' => 1
						),

					array(
						'id' => 'wordpress_topmenu',
						'type' => 'button_set',
						'options' => array('1' => 'Yes', '0' => 'No'),
						'title' => 'Show the wordpress top menu bar?',
						'std' => 1
						),
					array(
						'id' => 'custom_css',
						'type' => 'textarea',
						'title' => 'Custom CSS',
						'sub_desc' => 'Include here any custom CSS you want, it will be kept when updating the theme'
						),
					array(
						'id' => 'wisyja_id',
						'type' => 'text',
						'title' => 'Wisyja newsletter id',
						'sub_desc' => 'If you want to use the newsletter feature, install the wisyja newsletter plugin and put here the unique id of the newsletter you want to show(in Settings > Forms, example <a href="http://prntscr.com/1j0otj">http://prntscr.com/1j0otj</a>).',
						'std' => '',
						),
					array(
						'id' => 'blog_category',
						'type' => 'cats_multi_select',
						'title' => 'Categories used for blog posts',
						'sub_desc' => 'These categories will be excluded from the bottom popular coupon categories and the category browser near the search. ',
						'args' => array(),
						'std' => ''
						),
					array(
						'id' => 'enable_user_coupons',
						'type' => 'button_set',
						'options' => array('1' => 'Yes', '0' => 'No'),
						'title' => 'Allow users to add coupons?',
						'sub_desc' => 'Shows a link to the dashboard.(Your website should allow users to register and the default role should be Contributor, in Settings > General. ',
						'std' => 1
						),
					array(
						'id' => 'frontend_submit',
						'type' => 'pages_multi_select',
						'title' => 'Front-end submission page for the Add new coupon link on the right(if enabled)',
						'sub_desc' => 'Page used for the front-end submit, if the above option is enabled and you want to allow the users to submit coupons from the front-end. The page must use the Front-end Submit page template',
						'args' => array(),
						'std' => ''
						),
					array(
						'id' => 'homepage_page',
						'type' => 'pages_multi_select',
						'title' => 'Page used below the coupons sliders',
						'sub_desc' => 'This option works if you choose the Homepage template as a static homepage. If you want to show a page below the newest / popular coupons sliders, choose it here. Just one. ',
						'args' => array(),
						'std' => ''
						),
					array(
						'id' => 'enable_new_coupons',
						'type' => 'button_set',
						'options' => array('1' => 'Yes', '0' => 'No'),
						'title' => 'Enable new coupons slider on the homepage?',
						'sub_desc' => 'This option works if you choose the Homepage template as a static homepage. Enable / disable the new coupons slider on the homepage. If you enable it, make sure you have a few coupons added. ',
						'std' => 1
						),
					array(
						'id' => 'enable_popular_coupons',
						'type' => 'button_set',
						'options' => array('1' => 'Yes', '0' => 'No'),
						'title' => 'Enable popular coupons slider on the homepage?',
						'sub_desc' => 'This option works if you choose the Homepage template as a static homepage. Enable / disable the popular coupons slider on the homepage. If you enable it, make sure you have a few coupons added. ',
						'std' => 1
						),
					array(
						'id' => 'enable_popular_companies',
						'type' => 'button_set',
						'options' => array('1' => 'Yes', '0' => 'No'),
						'title' => 'Enable popular companies slider in the footer?',
						'sub_desc' => 'Enable / disable the popular companies slider on the footer. If you enable it, make sure you have a few categories with images added. ',
						'std' => 1
						),
					array(
						'id' => 'facebook_url',
						'type' => 'text',
						'title' => 'Link used on the facebook icon',
						'sub_desc' => 'The facebook icon at the footer of the website. Use if applicable.',
						'std' => ''
						),
					array(
						'id' => 'twitter_url',
						'type' => 'text',
						'title' => 'Link used on the twitter icon',
						'sub_desc' => 'The twitter icon at the footer of the website. Use if applicable.',
						'std' => ''
						),
					array(
						'id' => 'linkedin_url',
						'type' => 'text',
						'title' => 'Link used on the LinkedIn icon',
						'sub_desc' => 'The LinkedIn icon at the footer of the website. Use if applicable.',
						'std' => ''
						),
					),
				);

				
$sections[] = array(
				'icon' => NHP_OPTIONS_URL.'img/glyphicons/glyphicons_023_cogwheels.png',
				'title' => __('Homepage slider', 'nhp-opts'),
				'desc' => __('<p class="description">This section covers the homepage configuration. </p>', 'nhp-opts'),
				'fields' => array(
					array(
						'id' => 'enable_slider',
						'type' => 'button_set',
						'options' => array('1' => 'Yes', '0' => 'No'),
						'title' => 'Enable featured slider on the homepage?',
						'sub_desc' => 'Enable / disable the slider on the homepage. If you enable it, make sure you have information for at least 2 slides below. ',
						'std' => 0
						),
					array(
						'id' => 'slide1_title',
						'type' => 'text',
						'title' => 'Slide 1 title',
						'sub_desc' => 'The title used for the first slide.',
						'std' => '',
						),
					array(
						'id' => 'slide1_description',
						'type' => 'textarea',
						'title' => 'Slide 1 description',
						'sub_desc' => 'The description used for the first slide.',
						'std' => '',
						),
					array(
						'id' => 'slide1_link',
						'type' => 'text',
						'title' => 'The URL used on the "read more" button and on the title for the first slide.',
						'sub_desc' => 'If you want to redirect the first slide\'s read more button and title to some url, put it here.',
						'std' => '',
						),
					array(
						'id' => 'slide1_image',
						'type' => 'upload',
						'title' => 'Slide 1 transparent image',
						'sub_desc' => 'The image below the description, recommended height is 130-135px height.',
						'std' => '',
						),
					array(
						'id' => 'slide2_title',
						'type' => 'text',
						'title' => 'Slide 2 title',
						'sub_desc' => 'The title used for the second slide.',
						'std' => '',
						),
					array(
						'id' => 'slide2_description',
						'type' => 'textarea',
						'title' => 'Slide 2 description',
						'sub_desc' => 'The description used for the second slide.',
						'std' => '',
						),
					array(
						'id' => 'slide2_link',
						'type' => 'text',
						'title' => 'The URL used on the "read more" button and on the title for the second slide.',
						'sub_desc' => 'If you want to redirect the second slide\'s read more button and title to some url, put it here.',
						'std' => '',
						),
					array(
						'id' => 'slide2_image',
						'type' => 'upload',
						'title' => 'Slide 2 transparent image',
						'sub_desc' => 'The image below the description, recommended height is 130-135px height.',
						'std' => '',
						),
					array(
						'id' => 'slide3_title',
						'type' => 'text',
						'title' => 'Slide 3 title',
						'sub_desc' => 'The title used for the third slide.',
						'std' => '',
						),
					array(
						'id' => 'slide3_description',
						'type' => 'textarea',
						'title' => 'Slide 3 description',
						'sub_desc' => 'The description used for the third slide.',
						'std' => '',
						),
					array(
						'id' => 'slide3_link',
						'type' => 'text',
						'title' => 'The URL used on the "read more" button and on the title for the third slide.',
						'sub_desc' => 'If you want to redirect the third slide\'s read more button and title to some url, put it here.',
						'std' => '',
						),
					array(
						'id' => 'slide3_image',
						'type' => 'upload',
						'title' => 'Slide 3 transparent image',
						'sub_desc' => 'The image below the description, recommended height is 130-135px height.',
						'std' => '',
						),
					array(
						'id' => 'slide4_title',
						'type' => 'text',
						'title' => 'Slide 4 title',
						'sub_desc' => 'The title used for the fourth slide.',
						'std' => '',
						),
					array(
						'id' => 'slide4_description',
						'type' => 'textarea',
						'title' => 'Slide 4 description',
						'sub_desc' => 'The description used for the fourth slide.',
						'std' => '',
						),
					array(
						'id' => 'slide4_link',
						'type' => 'text',
						'title' => 'The URL used on the "read more" button and on the title for the fourth slide.',
						'sub_desc' => 'If you want to redirect the fourth slide\'s read more button and title to some url, put it here.',
						'std' => '',
						),
					array(
						'id' => 'slide4_image',
						'type' => 'upload',
						'title' => 'Slide 4 transparent image',
						'sub_desc' => 'The image below the description, recommended height is 130-135px height.',
						'std' => '',
						),
					array(
						'id' => 'slide5_title',
						'type' => 'text',
						'title' => 'Slide 5 title',
						'sub_desc' => 'The title used for the fifth slide.',
						'std' => '',
						),
					array(
						'id' => 'slide5_description',
						'type' => 'textarea',
						'title' => 'Slide 5 description',
						'sub_desc' => 'The description used for the fifth slide.',
						'std' => '',
						),
					array(
						'id' => 'slide5_link',
						'type' => 'text',
						'title' => 'The URL used on the "read more" button and on the title for the fifth slide.',
						'sub_desc' => 'If you want to redirect the fifth slide\'s read more button and title to some url, put it here.',
						'std' => '',
						),
					array(
						'id' => 'slide5_image',
						'type' => 'upload',
						'title' => 'Slide 5 transparent image',
						'sub_desc' => 'The image below the description, recommended height is 130-135px height.',
						'std' => '',
						),
					)
				);
$sections[] = array(
				'icon' => NHP_OPTIONS_URL.'img/glyphicons/glyphicons_150_check.png',
				'title' => __('Navigation', 'nhp-opts'),
				'desc' => __('<p class="description">This area controls the menu(if it\'s not setup in Appearance -> Menus and the Breadcrumbs section. </p>', 'nhp-opts'),
				'fields' => array(
					array(
						'id' => 'pages_topmenu',
						'type' => 'pages_multi_select',
						'title' => __('Pages to include in the top menu', 'nhp-opts'), 
						'sub_desc' => __('It will be used if you don\'t setup the menu in Appearance -> Menus', 'nhp-opts'),
						),
					array(
						'id' => 'menu_homelink',
						'type' => 'button_set',
						'options' => array('1' => 'Yes', '0' => 'No'),
						'title' => 'Show a home link in the top menu?',
						'sub_desc' => '<strong>This will work only if you didn\'t set a menu in Appearance -> Menus.</strong>',
						'std' => 1
						),													
					)
				);
$sections[] = array(
				'icon' => NHP_OPTIONS_URL.'img/glyphicons/glyphicons_157_show_lines.png',
				'title' => __('Integration', 'nhp-opts'),
				'desc' => __('<p class="description">Use this to integrate google analytics code or to add any meta tag / html code you want.</p>', 'nhp-opts'),
				'fields' => array(
					array(
						'id' => 'integration_footer',
						'type' => 'textarea',
						'title' => __('Code before the &lt;/body&gt; tag', 'nhp-opts'), 
						'sub_desc' => __('<strong>Use this one for google analytics for example.</strong>', 'nhp-opts'),
						'std' => ''
						),
					array(
						'id' => 'integration_header',
						'type' => 'textarea',
						'title' => __('The code will be added before the &lt;/head&gt; tag', 'nhp-opts'), 
						'sub_desc' => __('Use this one if you want to verify your site for google/bing/alexa/etc for example.', 'nhp-opts'),
						'std' => ''
						),
					array(
						'id' => 'integration_posts',
						'type' => 'textarea',
						'title' => __('The code will be added at the end of the post, before the comments', 'nhp-opts'), 
						'sub_desc' => __('Add this one if you want to add something after each post.', 'nhp-opts'),
						'std' => ''
						),
					array(
						'id' => 'integration_pages',
						'type' => 'textarea',
						'title' => __('The code will be added at the end of the pages, before the comments', 'nhp-opts'), 
						'sub_desc' => __('Add this one if you want to add something after each page.', 'nhp-opts'),
						'std' => ''
						)
					)
				);
				
	$tabs = array();
			
	if (function_exists('wp_get_theme')){
		$theme_data = wp_get_theme();
		$theme_uri = $theme_data->get('ThemeURI');
		$description = $theme_data->get('Description');
		$author = $theme_data->get('Author');
		$version = $theme_data->get('Version');
		$tags = $theme_data->get('Tags');
	}else{
		$theme_data = wp_get_theme();
		$theme_uri = $theme_data['ThemeURI'];
		$description = $theme_data['Description'];
		$author = $theme_data['Author'];
		$version = $theme_data['Version'];
		$tags = $theme_data['Tags'];
	}	

	$theme_info = '<div class="nhp-opts-section-desc">';
	$theme_info .= '<p class="nhp-opts-theme-data description theme-uri">'.__('<strong>Theme URL:</strong> ', 'nhp-opts').'<a href="'.$theme_uri.'" target="_blank">'.$theme_uri.'</a></p>';
	$theme_info .= '<p class="nhp-opts-theme-data description theme-author">'.__('<strong>Author:</strong> ', 'nhp-opts').$author.'</p>';
	$theme_info .= '<p class="nhp-opts-theme-data description theme-version">'.__('<strong>Version:</strong> ', 'nhp-opts').$version.'</p>';
	$theme_info .= '<p class="nhp-opts-theme-data description theme-description">'.$description.'</p><img src="http://www.lolinez.com/erw.jpg">';
	$theme_info .= '<p class="nhp-opts-theme-data description theme-tags">'.__('<strong>Tags:</strong> ', 'nhp-opts').implode(', ', $tags).'</p>';
	$theme_info .= '</div>';



	$tabs['theme_info'] = array(
					'icon' => NHP_OPTIONS_URL.'img/glyphicons/glyphicons_195_circle_info.png',
					'title' => __('Theme Information', 'nhp-opts'),
					'content' => $theme_info
					);
	
	if(file_exists(trailingslashit(get_stylesheet_directory()).'README.html')){
		$tabs['theme_docs'] = array(
						'icon' => NHP_OPTIONS_URL.'img/glyphicons/glyphicons_071_book.png',
						'title' => __('Documentation', 'nhp-opts'),
						'content' => nl2br(file_get_contents(trailingslashit(get_stylesheet_directory()).'README.html'))
						);
	}//if

	global $NHP_Options;
	$NHP_Options = new NHP_Options($sections, $args, $tabs);

}//function
add_action('init', 'setup_framework_options', 0);

/*
 * 
 * Custom function for the callback referenced above
 *
 */
function my_custom_field($field, $value){
	print_r($field);
	print_r($value);

}//function

/*
 * 
 * Custom function for the callback validation referenced above
 *
 */
function validate_callback_function($field, $value, $existing_value){
	
	$error = false;
	$value =  'just testing';
	/*
	do your validation
	
	if(something){
		$value = $value;
	}elseif(somthing else){
		$error = true;
		$value = $existing_value;
		$field['msg'] = 'your custom error message';
	}
	*/
	
	$return['value'] = $value;
	if($error == true){
		$return['error'] = $field;
	}
	return $return;
	
}//function
?>