<?php
/**
 * Include and setup custom metaboxes and fields.
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'cmb_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function cmb_sample_metaboxes( array $meta_boxes ) {

	$prefix = '_single_';

	$meta_boxes[] = array(
		'id'         => 'coupons_metabox',
		'title'      => 'Coupon details',
		'pages'      => array( 'coupons', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array( // Text Input
			    'name' => 'Value of the discount', // <label>
			    'desc'  => 'Put here the value of the discount(like $30 OFF or %10 discount)', // description
			    'id'    => $prefix . 'discount', // field id and name
			    'type'  => 'text', // type of field,
			    ),
			array( // Text Input
			    'name' => 'The date the coupon expires', // <label>
			    'desc'  => 'The expiration date of the coupon. Leave if empty if the coupon doesn\'t have an expiration date.', // description
			    'id'    => $prefix . 'date', // field id and name
			    'type'  => 'text_date_timestamp', // type of field,
			    ),
			array( // Text Input
			    'name' => 'Coupon code', // <label>
			    'desc'  => 'If it is a coupon code and not a discount link, put the code here, it will be hidden by default', // description
			    'id'    => $prefix . 'code', // field id and name
			    'type'  => 'text', // type of field,
			    ),
			array( // Text Input
			    'name' => 'Coupon URL', // <label>
			    'desc'  => 'If the discount is offered via a URL, put it here. You can only use either a coupon code or a URL, not both', // description
			    'id'    => $prefix . 'url', // field id and name
			    'type'  => 'text', // type of field,
			    ),
		)
	);

	$prefix = '_blog_';

	$meta_boxes[] = array(
		'id'         => 'blog_metabox',
		'title'      => 'Blog page template',
		'pages'      => array( 'page', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_on'    => array( 'key' => 'page-template', 'value' => 'page-template-blog.php'),
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array( // Text Input
			    'name' => 'Number of posts shown', // <label>
			    'desc'  => 'This is the number of blog posts shown on the blog page, before pagination', // description
			    'id'    => $prefix . 'nrposts', // field id and name
			    'type'  => 'text', // type of field,
			    'std' => 3
			    ),
			array(
				'name'    => 'Full width?',
				'desc'    => 'Is the page full width? Or should it show a sidebar?',
				'id'      => $prefix . 'fullwidth',
				'type'    => 'radio',
				'options' => array(
					array( 'name' => 'Full width', 'value' => 1, ),
					array( 'name' => 'With sidebar', 'value' => 2, ),
				),
				'std' => 2
			),
			array( // Text Input
			    'name' => 'Categories included in the blog page', // <label>
			    'desc'  => 'You can select one or more categories.', // description
			    'id'    => $prefix . 'categories', // field id and name
			    'type'  => 'taxonomy_multicheck', // type of field,
			    'taxonomy'	=> 'category', // Taxonomy Slug
			    ),
		)
	);

	$prefix = '_coupons_';

	$meta_boxes[] = array(
		'id'         => 'coupons_metabox',
		'title'      => 'Coupons page template',
		'pages'      => array( 'page', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_on'    => array( 'key' => 'page-template', 'value' => 'page-template-coupons.php'),
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array( // Text Input
			    'name' => 'Categories included in the coupons page', // <label>
			    'desc'  => 'You can select one or more categories.', // description
			    'id'    => $prefix . 'categories', // field id and name
			    'type'  => 'taxonomy_multicheck', // type of field,
			    'taxonomy'	=> 'category', // Taxonomy Slug
			    ),
		)
	);

	// Add other metaboxes as needed

	return $meta_boxes;
}

add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
/**
 * Initialize the metabox class.
 */
function cmb_initialize_cmb_meta_boxes() {

	if ( ! class_exists( 'cmb_Meta_Box' ) )
		require_once 'init.php';

}