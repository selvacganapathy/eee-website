<?php 
get_header();
?>
<div role="main">
            
    <div class="stripe-regular">
            
        <div class="row">                
                
            <div class="large-9 columns">
                                    
                <div class="coupons-wrapper">
                    <div class="row">
                        <div class="large-12 columns">
                            <h2>
                                <?php 
                                global $query_string;
                                query_posts($query_string . '&post_type=any');
                                _e('Coupons', 'Couponize');?> 
                                <span>- 
                                <?php 
                                    echo $wp_query->found_posts . ' ';
                                    _e('results', 'Couponize');
                                ?>
                                </span>
                            </h2>
                        </div>
                    </div>
                    
                    <ul class="rr items-landscape">
                        <?php 
                        if(have_posts() ) : while(have_posts() ) : the_post();
                            $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                            $discount = get_post_meta($post->ID, '_single_discount', true);
                            $date = get_post_meta($post->ID, '_single_date', true);
                            $code = get_post_meta($post->ID, '_single_code', true);
                            $url = get_post_meta($post->ID, '_single_url', true);   
                            $daysleft = round( ($date-time()) / 24 / 60 / 60);  
                        ?>
                            <li class="wrapper-3">
                                <?php if($post->post_type == 'coupons') { ?>
                                    <div <?php post_class('row');?>>
                                        <?php if($thumb != '') { ?>
                                            <div class="large-4 small-12 columns">
                                                <figure>
                                                    <a href="<?php the_permalink();?>">
                                                        <img src="<?php echo aq_resize($thumb, 200, 180, true); ?>" alt="<?php the_title();?>">
                                                    </a>
                                                </figure>
                                            </div>
                                        <?php } ?>
                                        <div class="large-<?php if($thumb != '') echo '5'; else echo '9';?> columns">
                                            <div class="col-middle">
                                                <h2 class="alt">
                                                    <a href="<?php the_permalink();?>"><?php the_title();?></a>
                                                </h2>
                                                <p class="text secondary">
                                                    <?php the_content('');?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="large-3 small-4 columns">
                                            <div class="col-right">
                                                <p class="value secondary"><?php echo $discount;?></p>
                                                <h6>
                                                    <?php 
                                                    if($date == '')
                                                        _e('VALID', 'Couponize');
                                                    else if($daysleft <= 0) 
                                                        _e('EXPIRED', 'Couponize'); 
                                                    else 
                                                        echo sprintf( _n('%d day left.', '%d days left.', $daysleft, 'Couponize'), $daysleft ); ?>
                                                </h6>
                                                <?php if($code != '') { 
                                                    if($url != '') {
                                                        $button = __('Reveal offer', 'Couponize');
                                                        $offerlink = home_url('/') . '?visit-offer=' . $post->ID;
                                                    }
                                                    else {
                                                        $button = __('Show Coupon', 'Couponize');
                                                        $offerlink = '#';
                                                    }
                                                    ?> 
                                                    <a <?php if($offerlink != '') echo 'target="_blank"';?> href="<?php echo $offerlink; ?>" class="input button red secondary responsive toggle_coupon">
                                                        <?php echo $button;?>
                                                    </a>
                                                    <span class="input button border high secondary responsive hidden_coupon">
                                                        <?php echo $code;?>
                                                    </span>
                                                <?php } 
                                                elseif($url != '') {
                                                    $offerlink = home_url('/') . '?visit-offer=' . $post->ID; ?>
                                                    <a target="_blank" href="<?php echo $offerlink; ?>" class="input button blue secondary responsive">
                                                        <?php _e('Visit offer', 'Couponize');?>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } elseif($post->post_type == 'post' || $post->post_type == 'page') { ?>
                                    <article <?php post_class('row');?>>
                                        <?php if($thumb != '') { ?>
                                            <div class="large-4 small-12 columns">
                                                <figure>
                                                    <a href="<?php the_permalink();?>">
                                                        <img src="<?php echo aq_resize($thumb, 200, 200, true); ?>" alt="<?php the_title();?>">
                                                    </a>
                                                </figure>
                                            </div>
                                        <?php } ?>
                                        <div class="large-<?php if($thumb != '') echo '8'; else echo '12';?> columns">
                                            <div class="col-middle">
                                                <h2 class="alt">
                                                    <a href="<?php the_permalink();?>"><?php the_title();?></a>
                                                </h2>
                                                <p class="rr subtitle">
                                                    <?php _e('Posted on', 'Couponize'); echo ' '; the_time("d/m/Y");?> 
                                                    <?php _e('by', 'Couponize'); echo ' '; the_author_posts_link(); 
                                                    echo ' | '; 
                                                    comments_popup_link(esc_html__('0 comments','Couponize'), esc_html__('1 comment','Couponize'), '% '.esc_html__('comments','Couponize')); ?>
                                                </p>
                                                <div class="text secondary"><?php global $more; $more = 0; the_content('');?></div>
                                                <a class="input button red secondary responsive fixed blue-h" href="<?php the_permalink();?>"><?php _e('Read more', 'Couponize');?></a>
                                            </div>
                                        </div>
                                    </article>
                                <?php } ?>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                        <?php get_template_part('includes/pagination');
                        endif; wp_reset_query();?>
                    <div style="clear: both"></div>
                </div>
                                    
            </div>
                
            <?php get_sidebar();?>
        </div>
    </div>
            
</div>
<?php get_footer();?>