<?php 
get_header();
$cat = get_query_var('cat');
$postsInCat = get_term_by('id', $cat,'category');
$postsInCat = $postsInCat->count;
?>
<div role="main">
            
    <div class="stripe-regular">
            
        <section class="company-box">
            <div class="row">
                <div class="column">
                    <div class="wrapper-3 tertiary">
                        <div class="row collapse">
                            <div class="large-8 columns">
                                <div class="splitter-right">
                                    <div class="inner">
                                        <div class="row collapse">
                                            <div class="large-4 columns">
                                                <figure>
                                                    <?php 
                                                    $image = get_option('taxonomy_' . $cat);
                                                    $image = $image['custom_term_meta'];
                                                    ?>
                                                    <img src="<?php echo aq_resize($image, 200, 110, true); ?>" alt="<?php single_cat_title( '', true );?>">
                                                </figure>
                                            </div>
                                            <div class="large-8 columns">
                                                <div class="space-left">
                                                    <h2 class="alt"><?php single_cat_title( '', true );?></h2>
                                                    <div class="text-content">
                                                        <?php echo category_description();?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="large-4 columns">
                                <div class="splitter-left">
                                    <div class="inner dark-bg">
                                        <div class="row">
                                            <div class="large-6 columns text-center">
                                                <h3><?php _e('Total Coupons', 'Couponize');?></h3>
                                            </div>
                                            <div class="large-6 columns">
                                                <div class="coupons-count">
                                                    <p class="value primary"><?php echo $postsInCat;?></p>
                                                    <h4><?php _e('coupons', 'Couponize');?></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inner">
                                        <ul class="rr micro-block-grid-2 small-block-grid-4 large-block-grid-2 photos">
                                        <?php
                                        $thumbs = array();
                                        $ids = array();
                                        $query = new WP_Query("cat=$cat&showposts=30&post_type=any"); 
                                        while($query->have_posts() ) : $query->the_post();
                                            if(count($thumbs) == 3) //we only need 4 thumbs
                                                break;
                                            $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                                            if($thumb && $thumb != '') {
                                                $thumbs[] = $thumb;
                                                $ids[] = $post->ID;
                                            }
                                        endwhile; wp_reset_postdata();
                                        for($i=0; $i<count($ids); $i++) { ?>
                                            <li>
                                                <a href="<?php echo get_permalink($ids[$i]);?>">
                                                    <img src="<?php echo aq_resize($thumbs[$i], 130, 130, true); ?>" alt="<?php single_cat_title( '', true ); echo ' '; _e('discounts', 'Couponize');?>">
                                                </a>
                                            </li>
                                        <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
            
        <div class="row">                
                
            <div class="large-9 columns">
                                    
                <div class="coupons-wrapper">
                    <div class="row">
                        <div class="large-6 columns">
                            <h2>
                                <?php _e('Coupons', 'Couponize');?> 
                                <span class="alt"><?php _e('by', 'Couponize');?></span> 
                                <a href="<?php echo get_category_link($cat);?>">
                                    <?php single_cat_title( '', true );?>
                                </a> 
                                <span>- 
                                <?php 
                                    echo $postsInCat . ' ';
                                    _e('results', 'Couponize');
                                ?>
                                </span>
                            </h2>
                        </div>
                        <div class="large-6 columns">
                            <div class="ui-links sort-items">
                                <a href="<?php echo add_query_arg('sort', 'thumbs');?>" <?php if(isset($_GET['sort']) && $_GET['sort'] == 'thumbs') echo 'class="current"';?>><?php _e('Most appreciated', 'Couponize');?></a>
                                <a href="<?php echo add_query_arg('sort', 'expiring');?>" <?php if(isset($_GET['sort']) && $_GET['sort'] == 'expiring') echo 'class="current"';?>><?php _e('Expiring soon', 'Couponize');?></a>
                                <a href="<?php echo add_query_arg('sort', 'popular');?>" <?php if(isset($_GET['sort']) && $_GET['sort'] == 'popular') echo 'class="current"';?>><?php _e('Popular', 'Couponize');?></a> | 
                                <a href="<?php echo add_query_arg('sort', 'newest');?>" <?php if(!isset($_GET['sort']) || $_GET['sort'] == 'newest') echo 'class="current"';?>><?php _e('Newest', 'Couponize');?></a>
                            </div>
                        </div>
                    </div>
                    
                    <ul class="rr items-landscape">
                        <?php 
                        if(have_posts() ) : while(have_posts() ) : the_post();
                            $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                            $discount = get_post_meta($post->ID, '_single_discount', true);
                            $date = get_post_meta($post->ID, '_single_date', true);
                            $code = get_post_meta($post->ID, '_single_code', true);
                            $url = get_post_meta($post->ID, '_single_url', true);   
                            $daysleft = round( ($date-time()) / 24 / 60 / 60);  
                        ?>
                            <li class="wrapper-3">
                                <?php if($post->post_type == 'coupons') { ?>
                                    <div <?php post_class('row');?>>
                                        <?php if($thumb != '') { ?>
                                            <div class="large-4 small-12 columns">
                                                <figure>
                                                    <a href="<?php the_permalink();?>">
                                                        <img src="<?php echo aq_resize($thumb, 200, 180, true); ?>" alt="<?php the_title();?>">
                                                    </a>
                                                </figure>
                                            </div>
                                        <?php } ?>
                                        <div class="large-<?php if($thumb != '') echo '5'; else echo '9';?> columns">
                                            <div class="col-middle">
                                                <h2 class="alt">
                                                    <a href="<?php the_permalink();?>"><?php the_title();?></a>
                                                </h2>
                                                <p class="text secondary">
                                                    <?php the_content('');?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="large-3 small-4 columns">
                                            <div class="col-right">
                                                <p class="value secondary"><?php echo $discount;?></p>
                                                <h6>
                                                    <?php
                                                    if($date == '')
                                                        _e('VALID', 'Couponize');
                                                    else if($daysleft <= 0) 
                                                        _e('EXPIRED', 'Couponize'); 
                                                    else 
                                                        echo sprintf( _n('%d day left.', '%d days left.', $daysleft, 'Couponize'), $daysleft ); ?>
                                                </h6>
                                                <?php if($code != '') { 
                                                    if($url != '') {
                                                        $button = __('Reveal offer', 'Couponize');
                                                        $offerlink = home_url('/') . '?visit-offer=' . $post->ID;
                                                    }
                                                    else {
                                                        $button = __('Show Coupon', 'Couponize');
                                                        $offerlink = '#';
                                                    }
                                                    ?> 
                                                    <a <?php if($offerlink != '') echo 'target="_blank"';?> href="<?php echo $offerlink; ?>" class="input button red secondary responsive toggle_coupon">
                                                        <?php echo $button;?>
                                                    </a>
                                                    <span class="input button border high secondary responsive hidden_coupon">
                                                        <?php echo $code;?>
                                                    </span>
                                                <?php } 
                                                elseif($url != '') {
                                                    $offerlink = home_url('/') . '?visit-offer=' . $post->ID; ?>
                                                    <a target="_blank" href="<?php echo $offerlink; ?>" class="input button blue secondary responsive">
                                                        <?php _e('Visit offer', 'Couponize');?>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } elseif($post->post_type == 'post') { ?>
                                    <article <?php post_class('row');?>>
                                        <?php if($thumb != '') { ?>
                                            <div class="large-4 small-12 columns">
                                                <figure>
                                                    <a href="<?php the_permalink();?>">
                                                        <img src="<?php echo aq_resize($thumb, 200, 200, true); ?>" alt="<?php the_title();?>">
                                                    </a>
                                                </figure>
                                            </div>
                                        <?php } ?>
                                        <div class="large-<?php if($thumb != '') echo '8'; else echo '12';?> columns">
                                            <div class="col-middle">
                                                <h2 class="alt">
                                                    <a href="<?php the_permalink();?>"><?php the_title();?></a>
                                                </h2>
                                                <p class="rr subtitle">
                                                    <?php _e('Posted on', 'Couponize'); echo ' '; the_time("d/m/Y");?> 
                                                    <?php _e('by', 'Couponize'); echo ' '; the_author_posts_link(); 
                                                    echo ' | '; 
                                                    comments_popup_link(esc_html__('0 comments','Couponize'), esc_html__('1 comment','Couponize'), '% '.esc_html__('comments','Couponize')); ?>
                                                </p>
                                                <div class="text secondary"><?php global $more; $more = 0; the_content('');?></div>
                                                <a class="input button red secondary responsive fixed blue-h" href="<?php the_permalink();?>"><?php _e('Read more', 'Couponize');?></a>
                                            </div>
                                        </div>
                                    </article>
                                <?php } ?>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                        <?php get_template_part('includes/pagination');
                        endif; wp_reset_query();?>
                    <div style="clear: both"></div>
                </div>
                                    
            </div>
                
            <?php get_sidebar();?>
        </div>
    </div>
            
</div>
<?php get_footer();?>