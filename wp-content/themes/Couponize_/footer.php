<?php global $teo_options;?>
<footer role="contentinfo">
            
    <?php if(is_plugin_active('wysija-newsletters/index.php') && isset($teo_options['wisyja_id']) && $teo_options['wisyja_id'] != '') { ?>
        <div class="newsletter-wrap stripe-white">
            <div class="row">
                <div class="large-3 columns">
                    <h3><?php _e('Newsletter Signup', 'Couponize');?></h3>
                </div>
                <?php echo do_shortcode('[wysija_form id="' . $teo_options['wisyja_id'] . '"]');?>
            </div>
        </div>
    <?php } ?>
            
    <?php if(isset($teo_options['enable_popular_companies']) && $teo_options['enable_popular_companies'] == 1) { ?>
        <div class="stripe-regular">
            <div class="row">
                <div class="column">                  
                    <h2><?php _e('Popular companies', 'Couponize');?></h2>
                </div>
            </div>
            <div class="row collapse">
                <div class="column">
                    <div class="popular-companies flexslider">
                        <ul class="rr slides">
                            <?php 
                            $args['hide_empty'] = 1;
                            $args['orderby'] = 'count';
                            $args['order'] = 'desc';
                            if(isset($teo_options['blog_category']) && $teo_options['blog_category'] != '')
                                $args['exclude'] = implode(",", $teo_options['blog_category']);
                            $categories = get_categories($args);
                            foreach($categories as $category) { 
                                $image = get_option('taxonomy_' . $category->cat_ID);
                                $image = $image['custom_term_meta'];
                            ?>
                                <li>
                                    <a href="<?php echo get_category_link( $category->term_id );?>" class="wrapper-5 small">
                                        <img src="<?php echo aq_resize($image, 130, 130, true); ?>" alt="<?php echo $category->name;?> coupons">
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
            
    <div class="stripe-dark pre-footer-wrap">
        <div class="row">
            <?php dynamic_sidebar("Footer sidebar");?> 
        </div>
    </div>
            
    <div class="stripe-darker footer-wrap">
        <div class="row">
            <div class="large-3 columns">
                <div class="logo">
                    <a href="<?php echo home_url();?>">
                        <?php 
                            if(isset($teo_options['logo']) && $teo_options['logo'] != '') 
                                echo '<img src="' . esc_url($teo_options['logo']) . '" />';
                            elseif(isset($teo_options['logo_text']) && $teo_options['logo_text'] != '') 
                                echo $teo_options['logo_text'];
                            else 
                                bloginfo('name');
                            ?>
                        </a>
                </div>
            </div>
            <div class="large-6 columns">
                <div class="copyright"><?php _e('Copyright', 'Couponize');?>  &copy; <?php echo date("Y "); bloginfo('name');?>, <?php _e('developed by', 'Couponize');?> <a href="http://teothemes.com"> TeoThemes</a>. <?php _e('All rights reserved', 'Couponize');?>.</div>
            </div>
            <div class="large-3 columns">
                <ul class="rr social">
                    <?php if(isset($teo_options['twitter_url']) && $teo_options['twitter_url'] != '') { ?>
                        <li><a href="<?php echo esc_url($teo_options['twitter_url']);?>" class="ir tw">Twitter</a></li>
                    <?php } ?>

                    <?php if(isset($teo_options['linkedin_url']) && $teo_options['linkedin_url'] != '') { ?>
                        <li><a href="<?php echo esc_url($teo_options['linkedin_url']);?>" class="ir in">LinkedIn</a></li>
                    <?php } ?>

                    <?php if(isset($teo_options['facebook_url']) && $teo_options['facebook_url'] != '') { ?>
                        <li><a href="<?php echo esc_url($teo_options['facebook_url']);?>" class="ir fb">Facebook</a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
            
</footer> 
          
<?php
if(isset($teo_options['integration_footer'])) echo $teo_options['integration_footer'] . PHP_EOL; 
    wp_footer(); 
?>
    </body>
</html>
