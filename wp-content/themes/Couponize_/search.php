<?php 
get_header();
?>
<div role="main">
            
    <div class="stripe-regular">
            
        <div class="row">                
                
            <div class="large-9 columns">
                                    
                <div class="coupons-wrapper">

                    <div class="row">
                        <div class="large-6 columns">
                            <h2>
                                <?php _e('Coupons', 'Couponize');?> 
                                <span class="alt"><?php _e(' found by ', 'Couponize');?></span> 
                                <a href="<?php echo get_category_link($cat);?>">
                                    <?php echo "'" . get_search_query() . "'";?>
                                </a> 
                                <span>- 
                                <?php 
                                    echo $wp_query->found_posts . ' ';
                                    _e('results', 'Couponize');
                                ?>
                                </span>
                            </h2>
                        </div>
                        <div class="large-6 columns">
                            <div class="ui-links sort-items">
                                <a href="<?php echo add_query_arg('sort', 'thumbs');?>" <?php if(isset($_GET['sort']) && $_GET['sort'] == 'thumbs') echo 'class="current"';?>><?php _e('Most appreciated', 'Couponize');?></a>
                                <a href="<?php echo add_query_arg('sort', 'expiring');?>" <?php if(isset($_GET['sort']) && $_GET['sort'] == 'expiring') echo 'class="current"';?>><?php _e('Expiring soon', 'Couponize');?></a>
                                <a href="<?php echo add_query_arg('sort', 'popular');?>" <?php if(isset($_GET['sort']) && $_GET['sort'] == 'popular') echo 'class="current"';?>><?php _e('Popular', 'Couponize');?></a> | 
                                <a href="<?php echo add_query_arg('sort', 'newest');?>" <?php if(!isset($_GET['sort']) || $_GET['sort'] == 'newest') echo 'class="current"';?>><?php _e('Newest', 'Couponize');?></a>
                            </div>
                        </div>
                    </div>
                    
                    <ul class="rr items-landscape">
                        <?php 
                        if(have_posts() ) : while(have_posts() ) : the_post();
                            $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                            $discount = get_post_meta($post->ID, '_single_discount', true);
                            $date = get_post_meta($post->ID, '_single_date', true);
                            $code = get_post_meta($post->ID, '_single_code', true);
                            $url = get_post_meta($post->ID, '_single_url', true);   
                            $daysleft = round( ($date-time()) / 24 / 60 / 60);  
                            $categories = get_the_category();
                            $category = '';
                            if($categories) { 
                                $category = $categories[0]->name;
                            }          
                        ?>
                            <li class="wrapper-3">
                                <div class="row">
                                    <?php if($thumb != '') { ?>
                                        <div class="large-4 small-12 columns">
                                            <figure>
                                                <a href="<?php the_permalink();?>">
                                                    <img src="<?php echo aq_resize($thumb, 200, 180, true); ?>" alt="<?php the_title();?>">
                                                </a>
                                            </figure>
                                        </div>
                                    <?php } ?>
                                    <div class="large-<?php if($thumb != '') echo '5'; else echo '9';?> columns">
                                        <div class="col-middle">
                                            <h2 class="alt">
                                                <a href="<?php the_permalink();?>">
                                                    <?php if($category != '') echo '<span class="orange">' . $category . '</span> - '; the_title();?>
                                                </a>
                                            </h2>
                                            <p class="text secondary">
                                                <?php the_content('');?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="large-3 small-4 columns">
                                        <div class="col-right">
                                            <p class="value secondary"><?php echo $discount;?></p>
                                            <h6>
                                                <?php 
                                                if($date == '') 
                                                    _e('VALID', 'Couponize');
                                                else if($daysleft <= 0) 
                                                    _e('EXPIRED', 'Couponize'); 
                                                else 
                                                    echo sprintf( _n('%d day left.', '%d days left.', $daysleft, 'Couponize'), $daysleft ); ?>
                                            </h6>
                                            <?php if($code != '') { 
                                                if($url != '') {
                                                    $button = __('Reveal offer', 'Couponize');
                                                    $offerlink = home_url('/') . '?visit-offer=' . $post->ID;
                                                }
                                                else {
                                                    $button = __('Show Coupon', 'Couponize');
                                                    $offerlink = '#';
                                                }
                                                ?> 
                                                <a <?php if($offerlink != '') echo 'target="_blank"';?> href="<?php echo $offerlink; ?>" class="input button red secondary responsive toggle_coupon">
                                                    <?php echo $button;?>
                                                </a>
                                                <span class="input button border high secondary responsive hidden_coupon">
                                                    <?php echo $code;?>
                                                </span>
                                            <?php } 
                                            elseif($url != '') {
                                                $offerlink = home_url('/') . '?visit-offer=' . $post->ID; ?>
                                                <a target="_blank" href="<?php echo $offerlink; ?>" class="input button blue secondary responsive">
                                                    <?php _e('Visit offer', 'Couponize');?>
                                                </a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                        <?php get_template_part('includes/pagination');
                        else : ?>
                            <p class="text">
                                <?php 
                                    _e('No results match your search criteria ', 'Couponize');
                                    echo '<span class="orange">' . get_search_query() . '</span>. '; 
                                    _e('Try again.', 'Couponize');
                                ?>
                            </p> 
                            <?php
                        endif; wp_reset_query();?>
                    <div style="clear: both"></div>
                </div>
                                    
            </div>
                
            <?php get_sidebar();?>
        </div>
    </div>
            
</div>
<?php get_footer();?>