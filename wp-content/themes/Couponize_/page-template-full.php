<?php 
/*
Template name: Full width page 
*/
get_header();
the_post();
?>
<div role="main">     
    <div class="stripe-regular">                
        <div class="row">                
                
            <div class="large-12 columns">
                                    
                <article class="blog-post-content">
                    <section class="article-main-content">
                        <div class="wrapper-3 primary">
                            <div class="row">
                                <div class="column">
                                    <heading>
                                        <?php $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                                        if($thumb != '') { ?>
                                            <figure>
                                                <img src="<?php echo aq_resize($thumb, 900, 350, true); ?>" alt="<?php the_title();?>">
                                            </figure>
                                        <?php } ?>
                                        <h2 class="alt"><?php the_title();?></h2>
                                        <p class="rr subtitle">
                                            <?php _e('Posted on', 'Couponize'); echo ' '; the_time("d/m/Y");?> 
                                            <?php _e('by', 'Couponize'); echo ' '; the_author_posts_link(); 
                                            echo ' | '; 
                                            comments_popup_link(esc_html__('0 comments','Couponize'), esc_html__('1 comment','Couponize'), '% '.esc_html__('comments','Couponize')); ?>
                                        </p>
                                    </heading>

                                    <div class="text-content">
                                        <?php the_content('');?>
                                    </div>

                                    <a href="#" class="input button dark secondary responsive fixed share-button right">Share</a>

                                    <!-- AddThis Button BEGIN -->
                                    <div class="share_toggle addthis_toolbox addthis_default_style">
                                        <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                        <a class="addthis_button_tweet"></a>
                                        <a class="addthis_button_pinterest_pinit"></a>
                                        <a class="addthis_counter addthis_pill_style"></a>
                                    </div>
                                    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5133cbfc3c9054b8"></script>
                                    <!-- AddThis Button END -->
                                </div>
                            </div>
                        </div>

                    </section>

                    <?php comments_template('', true);?>
                    
                  </article>
                  
            </div>
                            
        </div>
    </div>
            
</div>
<?php get_footer();?>