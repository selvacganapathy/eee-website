<?php
add_shortcode('one_third','voxis_one_third');
function voxis_one_third($atts, $content = null){
	$output = '<div class="large-4 columns">';
	$output .= do_shortcode($content);
	$output .= '</div>';
	return $output;
}

add_shortcode('one_half','voxis_one_half');
function voxis_one_half($atts, $content = null){
	$output = '<div class="large-6 columns">';
	$output .= do_shortcode($content);
	$output .= '</div>';
	return $output;
}

add_shortcode('two_thirds','voxis_two_thirds');
function voxis_two_thirds($atts, $content = null){
	$output = '<div class="large-8 columns">';
	$output .= do_shortcode($content);
	$output .= '</div>';
	return $output;
}

add_shortcode('one_fourth','voxis_one_fourth');
function voxis_one_fourth($atts, $content = null){
	$output = '';
	$output .= '<div class="large-3 columns">';
	$output .= do_shortcode($content);
	$output .= '</div>';
	return $output;
}

add_shortcode('button', 'voxis_button');
function voxis_button($atts, $content=null) {
	extract(shortcode_atts(array(
		'color' => '',
		'url' => '',
		'newwindow' => '',
		'icon' => ''
	), $atts));
	$output = '';
	$extra = '';
	$extra2 = '';
	$class = 'input button primary ';
	if($color == 'red' || $color == 'blue' || $color == 'dark')
		$class .= $color . ' ';
	elseif($color[0] == '#')
		$extra2 = ' style="background-image: none; background-color: ' . $color . '" ';
	if($url !== '') {
		if($newwindow !== '')
			$output = '<a ' . $extra2 . ' class="' . $class . '" target="_blank" href="' . esc_url($url) . '">';
		else
			$output = '<a ' . $extra2 . ' class="' . $class . '" href="' . esc_url($url) . '">';
		$output .= $extra . $content . '</a>';
	}
	else
		$output = '<span ' . $extra2 . ' class="' . $class . '">' . $extra . $content . '</span>';

    return $output;
}

add_shortcode('testimonials_slider', 'teo_testimonials_slider');
function teo_testimonials_slider($atts, $content=null) {
	$content = do_shortcode($content);
	$output = '<div class="testimonials-box flexslider">
        <ul class="rr slides">';
    $output .= $content;
    $output .= '</ul>
    </div>' . PHP_EOL;
    return $output;
}
add_shortcode('testimonial', 'teo_testimonial');
function teo_testimonial($atts, $content=null) {
	extract(shortcode_atts(array(
		'author' => ''
	), $atts));

	$content = do_shortcode($content);
	$output = '<li>
        <div class="testimonial">
            <div class="wrapper-5 bubble">
                <p class="text small rr">' . $content . '</p>
            </div>
    ';
    if($author !== '') 
    	$output .= '<h5 class="alt text-right">' . $author . '</h5>';
    $output .= '
        </div>
    </li>' . PHP_EOL;
    return $output;
}

add_shortcode('clear', 'teo_clear');
function teo_clear($atts, $content=null) {
	return '<div style="clear: both"></div>';
}
?>