<?php

// PopularCategories Widget
class PopularCategories extends WP_Widget
{
    function PopularCategories(){
    $widget_ops = array('description' => 'Shows the most popular coupon companies.');
    $control_ops = array('width' => 200, 'height' => 300);
    parent::__construct(false,$name='[Couponize] Popular companies',$widget_ops,$control_ops);
    }

  /* Displays the Widget in the front-end */
    function widget($args, $instance){
    extract($args);

    $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
    $number_posts = $instance['number_posts'];
    $exclude = $instance['categories'];
    if($number_posts <= 0) 
        $number_posts = 10;

    echo $before_widget; 

    if ( $title != '' )
        echo $before_title . $title . $after_title;

    echo '<ul class="rr inne sidebar-list popular-companies-list">';
    $args = array();
    if(!empty($exclude) )
        $exclude = implode(",", $exclude);
    $categories = get_categories('orderby=count&order=desc&exclude=' . $exclude . '&number=' . $number_posts);
    foreach($categories as $category) { ?>
        <li class="cf">
            <a href="<?php echo get_category_link( $category->term_id );?>">
                <?php echo $category->name;?> 
                <span class="count-bullet"><?php echo $category->category_count;?></span>
            </a>
        </li>
    <?php }
    echo '</ul>';

    echo $after_widget;
  }

  /*Saves the settings. */
    function update($new_instance, $old_instance){
    $instance =  array();
    $instance['title'] = esc_attr($new_instance['title']);
    $instance['number_posts'] = (int)$new_instance['number_posts'];
    $instance['categories'] = $new_instance['categories'];

    return $instance;
  }

  /*Creates the form for the widget in the back-end. */
    function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('number_posts'=> 10, 'title' => '', 'categories' => array() ) );

    $title = esc_attr($instance['title']);
    $number_posts = (int) $instance['number_posts'];
    $categories = (array) $instance['categories'];
    
    echo '<p><label for="' . $this->get_field_id('title') . '">' . 'Title: ' . '</label><input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" value="'. esc_textarea($title)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('number_posts') . '">' . 'Number of posts: ' . '</label><input id="' . $this->get_field_id('number_posts') . '" name="' . $this->get_field_name('number_posts') . '" value="'. esc_textarea($number_posts)  . '" /></p>';
    $cats = get_categories('hide_empty=0');
    ?>
    <p>
        <label>Categories to exclude(if applicable): </label> <br />
        <?php foreach( $cats as $category ) { ?>
            <label>
                <input type="checkbox" name="<?php echo $this->get_field_name('categories'); ?>[]" value="<?php echo $category->cat_ID; ?>"  <?php if(in_array( $category->cat_ID, $categories ) ) echo 'checked="checked" '; ?> /> 
                <?php echo $category->cat_name; ?>
            </label> <br />
        <?php } ?>
    </p> 
    <?php
  }

}// end NewsInPictures class

// PopularPosts Widget
class ReviewsSlider extends WP_Widget
{
    function ReviewsSlider(){
    $widget_ops = array('description' => 'Slider with reviews.');
    $control_ops = array('width' => 200, 'height' => 300);
    parent::__construct(false,$name='[Couponizer] Reviews slider',$widget_ops,$control_ops);
    }

  /* Displays the Widget in the front-end */
    function widget($args, $instance){
    extract($args);

    $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
    $review1 = $instance['review1'];
    $author1 = $instance['author1'];
    $review2 = $instance['review2'];
    $author2 = $instance['author2'];
    $review3 = $instance['review3'];
    $author3 = $instance['author3'];
    $review4 = $instance['review4'];
    $author4 = $instance['author4'];
    $review5 = $instance['review5'];
    $author5 = $instance['author5'];

    echo $before_widget; 

    if ( $title != '' )
        echo $before_title . $title . $after_title; ?>

    <div class="testimonials-box">
        <ul class="rr slides">
            <?php if($review1 != '') { ?>
                <li>
                    <div class="testimonial">
                        <div class="wrapper-5 bubble">
                            <p class="text small rr">
                                  <?php echo esc_attr($review1);?>
                            </p>
                        </div>
                        <?php if($author1 != '') { ?> 
                            <h5 class="alt text-right"><?php echo esc_attr($author1);?></h5>
                        <?php } ?>
                    </div>
                </li>
            <?php } ?>

            <?php if($review2 != '') { ?>
                <li>
                    <div class="testimonial">
                        <div class="wrapper-5 bubble">
                            <p class="text small rr">
                                  <?php echo esc_attr($review2);?>
                            </p>
                        </div>
                        <?php if($author2 != '') { ?> 
                            <h5 class="alt text-right"><?php echo esc_attr($author2);?></h5>
                        <?php } ?>
                    </div>
                </li>
            <?php } ?>

            <?php if($review3 != '') { ?>
                <li>
                    <div class="testimonial">
                        <div class="wrapper-5 bubble">
                            <p class="text small rr">
                                  <?php echo esc_attr($review3);?>
                            </p>
                        </div>
                        <?php if($author3 != '') { ?> 
                            <h5 class="alt text-right"><?php echo esc_attr($author3);?></h5>
                        <?php } ?>
                    </div>
                </li>
            <?php } ?>

            <?php if($review4 != '') { ?>
                <li>
                    <div class="testimonial">
                        <div class="wrapper-5 bubble">
                            <p class="text small rr">
                                  <?php echo esc_attr($review4);?>
                            </p>
                        </div>
                        <?php if($author4 != '') { ?> 
                            <h5 class="alt text-right"><?php echo esc_attr($author4);?></h5>
                        <?php } ?>
                    </div>
                </li>
            <?php } ?>

            <?php if($review5 != '') { ?>
                <li>
                    <div class="testimonial">
                        <div class="wrapper-5 bubble">
                            <p class="text small rr">
                                  <?php echo esc_attr($review5);?>
                            </p>
                        </div>
                        <?php if($author5 != '') { ?> 
                            <h5 class="alt text-right"><?php echo esc_attr($author5);?></h5>
                        <?php } ?>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>

    <?php echo $after_widget;
  }

  /*Saves the settings. */
    function update($new_instance, $old_instance){
    $instance =  array();
    $instance['title'] = esc_attr($new_instance['title']);
    $instance['review1'] = $new_instance['review1'];
    $instance['author1'] = $new_instance['author1'];
    $instance['review2'] = $new_instance['review2'];
    $instance['author2'] = $new_instance['author2'];
    $instance['review3'] = $new_instance['review3'];
    $instance['author3'] = $new_instance['author3'];
    $instance['review4'] = $new_instance['review4'];
    $instance['author4'] = $new_instance['author4'];
    $instance['review5'] = $new_instance['review5'];
    $instance['author5'] = $new_instance['author5'];

    return $instance;
  }

  /*Creates the form for the widget in the back-end. */
    function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('review1'=> '', 'author1' => '', 'review2'=> '', 'author2' => '', 'review3'=> '', 'author3' => '', 'review4'=> '', 'author4' => '', 'review5'=> '', 'author5' => '', 'title' => '' ) );

    $title = esc_attr($instance['title']);
    $review1 = esc_attr($instance['review1']);
    $author1 = esc_attr($instance['author1']);
    $review2 = esc_attr($instance['review2']);
    $author2 = esc_attr($instance['author2']);
    $review3 = esc_attr($instance['review3']);
    $author3 = esc_attr($instance['author3']);
    $review4 = esc_attr($instance['review4']);
    $author4 = esc_attr($instance['author4']);
    $review5 = esc_attr($instance['review5']);
    $author5 = esc_attr($instance['author5']);
    
    echo '<p><label for="' . $this->get_field_id('title') . '">' . 'Title: ' . '</label><br /><input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" value="'. esc_textarea($title)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('review1') . '">' . 'Review #1: ' . '</label><input class="widefat" id="' . $this->get_field_id('review1') . '" name="' . $this->get_field_name('review1') . '" value="'. esc_textarea($review1)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('author1') . '">' . 'Author of review #1: ' . '</label><input id="' . $this->get_field_id('author1') . '" name="' . $this->get_field_name('author1') . '" value="'. esc_textarea($author1)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('review2') . '">' . 'Review #2(if used): ' . '</label><input class="widefat" id="' . $this->get_field_id('review2') . '" name="' . $this->get_field_name('review2') . '" value="'. esc_textarea($review2)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('author2') . '">' . 'Author of review #2(if used): ' . '</label><input id="' . $this->get_field_id('author2') . '" name="' . $this->get_field_name('author2') . '" value="'. esc_textarea($author2)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('review3') . '">' . 'Review #3(if used): ' . '</label><input class="widefat" id="' . $this->get_field_id('review3') . '" name="' . $this->get_field_name('review3') . '" value="'. esc_textarea($review3)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('author3') . '">' . 'Author of review #3(if used): ' . '</label><input id="' . $this->get_field_id('author3') . '" name="' . $this->get_field_name('author3') . '" value="'. esc_textarea($author3)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('review4') . '">' . 'Review #4(if used): ' . '</label><input class="widefat" id="' . $this->get_field_id('review4') . '" name="' . $this->get_field_name('review4') . '" value="'. esc_textarea($review4)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('author4') . '">' . 'Author of review #4(if used): ' . '</label><input id="' . $this->get_field_id('author4') . '" name="' . $this->get_field_name('author4') . '" value="'. esc_textarea($author4)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('review5') . '">' . 'Review #5(if used): ' . '</label><input class="widefat" id="' . $this->get_field_id('review5') . '" name="' . $this->get_field_name('review5') . '" value="'. esc_textarea($review5)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('author5') . '">' . 'Author of review #5(if used): ' . '</label><input id="' . $this->get_field_id('author5') . '" name="' . $this->get_field_name('author5') . '" value="'. esc_textarea($author5)  . '" /></p>';
  }

}// end PopularPosts class

// RandomPosts Widget
class RandomPosts extends WP_Widget
{
    function RandomPosts(){
    $widget_ops = array('description' => 'Shows some random posts from your website.');
    $control_ops = array('width' => 200, 'height' => 300);
    parent::__construct(false,$name='[Voxis] Random posts',$widget_ops,$control_ops);
    }

  /* Displays the Widget in the front-end */
    function widget($args, $instance){
    extract($args);

    $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);
    $number_posts = $instance['number_posts'];

    echo $before_widget; 

    if ( $title != '' )
        echo $before_title . $title . $after_title;

    echo '
    <div class="list">
        <ul>';
    $args = array();
    $args['posts_per_page'] = $number_posts;
    $args['orderby'] = 'rand';
    $query = new WP_Query($args);
    while($query->have_posts() ) : $query->the_post(); global $post; ?>
        <li>
            <a title="<?php the_title();?>" href="<?php the_permalink();?>">
                <figure>
                    <?php if(has_post_thumbnail() ) { ?>
                        <a href="<?php the_permalink();?>">
                            <img src="<?php echo aq_resize(wp_get_attachment_url( get_post_thumbnail_id($post->ID) ), 42, 42, true); ?>" alt="<?php the_title();?>" />
                        </a>
                    <?php } else { ?>
                        <a href="<?php the_permalink();?>">
                            <img src="<?php echo get_template_directory_uri() . '/img/missing_56.png';?>" alt="<?php the_title();?>" />
                        </a>
                    <?php } ?>
                </figure>
                <p>
                    <a href="<?php the_permalink();?>"><?php the_title();?></a> <br /> 
                    <span> <?php the_time("F d, Y");?>, <?php comments_popup_link(esc_html__('0 comments','Voxis'), esc_html__('1 comment','Voxis'), '% '.esc_html__('comments','Voxis')); ?> </span>
                </p>
            </a>
        </li>
    <?php endwhile; wp_reset_postdata();
    echo '</ul>
    </div>';

    echo $after_widget;
  }

  /*Saves the settings. */
    function update($new_instance, $old_instance){
    $instance =  array();
    $instance['title'] = esc_attr($new_instance['title']);
    $instance['number_posts'] = (int)$new_instance['number_posts'];

    return $instance;
  }

  /*Creates the form for the widget in the back-end. */
    function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('number_posts'=> 5, 'title' => '' ) );

    $title = esc_attr($instance['title']);
    $number_posts = (int) $instance['number_posts'];
    
    echo '<p><label for="' . $this->get_field_id('title') . '">' . 'Title: ' . '</label><input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" value="'. esc_textarea($title)  . '" /></p>';
    echo '<p><label for="' . $this->get_field_id('number_posts') . '">' . 'Number of posts: ' . '</label><input id="' . $this->get_field_id('number_posts') . '" name="' . $this->get_field_name('number_posts') . '" value="'. esc_textarea($number_posts)  . '" /></p>';
  }

}// end RandomPosts class

// FilterCoupons Widget
class FilterCoupons extends WP_Widget
{
    function FilterCoupons(){
    $widget_ops = array('description' => 'Filtering options for coupons.');
    $control_ops = array('width' => 200, 'height' => 300);
    parent::__construct(false,$name='[Couponize] Filter coupons',$widget_ops,$control_ops);
    }

  /* Displays the Widget in the front-end */
    function widget($args, $instance){ 
    wp_enqueue_style( 'jquery-ui-theme', get_template_directory_uri() . '/css/jquery-ui-theme.css', array(), '1.0');
    extract($args);

    $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title']);

    echo $before_widget; 

    if ( $title != '' )
        echo $before_title . $title . $after_title;

    ?>

    <form action="<?php echo get_template_directory_uri();?>/page-template-coupons.php" method="get">
        <h5 class="inner"><?php _e('Days left', 'Couponize');?></h5>    
        <div class="inner">        
            <div class="filter-days">
                <div class="bg-bar">
                    <div class="slider"></div>
                </div>
                            
                <div class="info">
                    <input class="input-min hidden" type="text" class="hidden" value="0" name="min" />
                    <input class="input-max hidden" type="text" class="hidden" value="80" name="max" />
                    <span class="begin"></span>
                    <span class="end"></span>
                </div>
            </div>            
        </div>
                        
        <h5 class="inner"><?php _e('Companies', 'Couponize');?></h5>
                        
        <ul class="rr inner checklist">
            <?php 
            $categories = get_categories();
            foreach($categories as $category) { ?>
                <li class="cf">
                    <label>
                        <span class="checkbox"><input name="categories[]" value="<?php echo $category->term_id;?>" type="checkbox"></span>
                        <span><?php echo $category->name;?> </span>
                    </label>
                    <p class="rr count-bullet"><?php echo $category->category_count;?></p>
                </li>
            <?php } ?>
        </ul>
        <br />
        <input class="input button dark secondary responsive" type="submit" value="<?php _e('Show coupons', 'Couponize');?>" />
    </form>
        <?php 

    echo $after_widget;
  }

  /*Saves the settings. */
    function update($new_instance, $old_instance){
    $instance =  array();
    $instance['title'] = esc_attr($new_instance['title']);

    return $instance;
  }

  /*Creates the form for the widget in the back-end. */
    function form($instance){
    //Defaults
    $instance = wp_parse_args( (array) $instance, array('number_posts'=> 5, 'title' => '', 'categories' => array() ) );

    $title = esc_attr($instance['title']);
    $number_posts = (int) $instance['number_posts'];
    $categories = (array) $instance['categories'];
    
    echo '<p><label for="' . $this->get_field_id('title') . '">' . 'Title: ' . '</label><input id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" value="'. esc_textarea($title)  . '" /></p>';
  }

}// end RecentPosts class

function VoxisWidgets() {
  register_widget('PopularCategories');
  register_widget('ReviewsSlider');
  register_widget('FilterCoupons');
}

add_action('widgets_init', 'VoxisWidgets');