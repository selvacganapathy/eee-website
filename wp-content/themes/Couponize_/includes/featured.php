<?php global $teo_options; 
if(isset($teo_options['enable_slider']) && $teo_options['enable_slider'] == 1) { ?>
    <div class="main-slider-wrap">
        <div class="row">
            <div class="column">
                <div class="main-slider flexslider white">
                    <ul class="slides">
                        <?php 
                        for($i=1; $i<=5; $i++) {
                            $prefix = 'slide' . $i . '_';
                            $title = $prefix . 'title';
                            $description = $prefix . 'description';
                            $link = $prefix . 'link';
                            $image = $prefix . 'image';
                            if( (isset($teo_options[$title]) && $teo_options[$title] != '') ||
                                      (isset($teo_options[$description]) && $teo_options[$description] != '') ) { ?>
                                <li>
                                    <?php if(isset($teo_options[$title]) && $teo_options[$title] != '') { ?>
                                        <?php if(isset($teo_options[$link]) && $teo_options[$link] != '') { ?>
                                            <a href="<?php echo esc_url($teo_options[$link]);?>">
                                                <p class="h1"><?php echo $teo_options[$title];?></p>
                                            </a>
                                        <?php } else { ?>
                                            <p class="h1"><?php echo $teo_options[$title];?></p>
                                        <?php }
                                    } ?>
                                    <div class="row">
                                        <div class="large-8 columns large-centered">
                                            <div class="content">
                                                <?php if(isset($teo_options[$description]) && $teo_options[$description] != '') { ?>
                                                    <p class="caption">
                                                        <?php 
                                                        echo $teo_options[$description];
                                                        if(isset($teo_options[$link]) && $teo_options[$link] != '') { ?>
                                                            <a href="<?php echo esc_url($teo_options[$link]);?>" class="ir icon tag">Learn more</a>
                                                        <?php } ?>
                                                    </p>
                                                <?php } ?>
                                                
                                                <?php if(isset($teo_options[$image]) && $teo_options[$image] != '') { ?>
                                                    <figure>
                                                        <img src="<?php echo esc_url($teo_options[$image]);?>" alt="<?php echo $teo_options[$title];?>">
                                                    </figure>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php } 
                        } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<?php } ?>