<?php 
/*
Template name: Coupons page 
*/
if(isset($_GET['categories']) || isset($_GET['min']) ) {
    $absolute_path = __FILE__;
    $path_to_file = explode( 'wp-content', $absolute_path );
    $path_to_wp = $path_to_file[0];

    // Access WordPress
    require_once( $path_to_wp . '/wp-load.php' );
}
get_header();
?>
<div role="main">
            
    <div class="stripe-regular">
            
        <div class="row">                
                
            <div class="large-9 columns">
                                    
                <div class="coupons-wrapper">

                    <?php
                        $paged = is_front_page() ? get_query_var( 'page' ) : get_query_var( 'paged' );
                        $args['paged'] = $paged;
                        $args['post_type'] = 'coupons';
                        if(isset($_GET['sort']) && $_GET['sort'] == 'popular') {
                            $args['orderby'] = 'comment_count';
                            $args['order'] = ' desc';
                        }
                        elseif(isset($_GET['sort']) && $_GET['sort'] == 'thumbs') {
                            $args['gdsr_sort'] = 'thumbs';
                            $args['sort_order'] = ' desc';
                            $args['meta_query'] = array(
                                    array(
                                        'key' => '_single_date',
                                        'value' =>  strtotime("now"),
                                        'compare' => '>=',
                                        'type' => 'UNSIGNED'
                                    )
                                );
                        }
                        elseif(isset($_GET['sort']) && $_GET['sort'] == 'expiring') {
                            $args['meta_query'] = array(
                                    array(
                                        'key' => '_single_date',
                                        'value' =>  strtotime("now"),
                                        'compare' => '>=',
                                        'type' => 'UNSIGNED'
                                    )
                                );
                            $args['meta_key'] = '_single_date';
                            $args['orderby'] = 'meta_value';
                            $args['order'] = 'asc';
                        }
                        if(isset($_GET['categories']) || isset($_GET['min']) ) {
                            if(isset($_GET['categories']) && count($_GET['categories']) > 0)
                                $args['category__in'] = $_GET['categories'];
                            $min = (int)$_GET['min'];
                            $max = (int)$_GET['max'];
                            $minarg = strtotime("now") + $min * 86400; //converting days to timestamp
                            $maxarg = strtotime("now") + $max * 86400; //converting days to timestamp
                            $args['meta_query'] = array(
                                    array(
                                        'key' => '_single_date',
                                        'value' => array( $minarg, $maxarg ),
                                        'compare' => 'BETWEEN',
                                        'type' => 'UNSIGNED'
                                    )
                                );
                        }
                        else {
                            $categories = get_post_meta($post->ID, '_coupons_categories', true);
                            if($categories)
                                $args['category__in'] = $categories; 
                        }
                        query_posts($args);

                    ?>
                    <div class="row">
                        <div class="large-6 columns">
                            <h2>
                                <?php _e('Coupons', 'Couponize');?> 
                                <span>- 
                                <?php 
                                    echo $wp_query->found_posts . ' ';
                                    _e('results', 'Couponize');
                                ?>
                                </span>
                            </h2>
                        </div>
                        <div class="large-6 columns">
                            <div class="ui-links sort-items">
                                <a href="<?php echo add_query_arg('sort', 'thumbs');?>" <?php if(isset($_GET['sort']) && $_GET['sort'] == 'thumbs') echo 'class="current"';?>><?php _e('Most appreciated', 'Couponize');?></a>
                                <a href="<?php echo add_query_arg('sort', 'expiring');?>" <?php if(isset($_GET['sort']) && $_GET['sort'] == 'expiring') echo 'class="current"';?>><?php _e('Expiring soon', 'Couponize');?></a>
                                <a href="<?php echo add_query_arg('sort', 'popular');?>" <?php if(isset($_GET['sort']) && $_GET['sort'] == 'popular') echo 'class="current"';?>><?php _e('Popular', 'Couponize');?></a> | 
                                <a href="<?php echo add_query_arg('sort', 'newest');?>" <?php if(!isset($_GET['sort']) || $_GET['sort'] == 'newest') echo 'class="current"';?>><?php _e('Newest', 'Couponize');?></a>
                            </div>
                        </div>
                    </div>
                    
                    <ul class="rr items-landscape">
                        <?php 
                        if(have_posts() ) : while(have_posts() ) : the_post();
                            $thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                            $discount = get_post_meta($post->ID, '_single_discount', true);
                            $date = get_post_meta($post->ID, '_single_date', true);
                            $code = get_post_meta($post->ID, '_single_code', true);
                            $url = get_post_meta($post->ID, '_single_url', true);   
                            $daysleft = round( ($date-time()) / 24 / 60 / 60);  
                            $categories = get_the_category();
                            $category = '';
                            if($categories) { 
                                $category = $categories[0]->name;
                            }          
                        ?>
                            <li class="wrapper-3">
                                <div <?php post_class('row');?>>
                                    <?php if($thumb != '') { ?>
                                        <div class="large-4 small-12 columns">
                                            <figure>
                                                <a href="<?php the_permalink();?>">
                                                    <img src="<?php echo aq_resize($thumb, 200, 180, true); ?>" alt="<?php the_title();?>">
                                                </a>
                                            </figure>
                                        </div>
                                    <?php } ?>
                                    <div class="large-<?php if($thumb != '') echo '5'; else echo '9';?> columns">
                                        <div class="col-middle">
                                            <h2 class="alt">
                                                <a href="<?php the_permalink();?>">
                                                    <?php if($category != '') echo '<span class="orange">' . $category . '</span> - '; the_title();?>
                                                </a>
                                            </h2>
                                            <p class="text secondary">
                                                <?php the_content('');?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="large-3 small-4 columns">
                                        <div class="col-right">
                                            <p class="value secondary"><?php echo $discount;?></p>
                                            <h6>
                                                <?php
                                                if($date == '')
                                                    _e('VALID', 'Couponize');
                                                else if($daysleft <= 0) 
                                                    _e('EXPIRED', 'Couponize'); 
                                                else 
                                                    echo sprintf( _n('%d day left.', '%d days left.', $daysleft, 'Couponize'), $daysleft ); ?>
                                            </h6>
                                            <?php if($code != '') { 
                                                if($url != '') {
                                                    $button = __('Reveal offer', 'Couponize');
                                                    $offerlink = home_url('/') . '?visit-offer=' . $post->ID;
                                                }
                                                else {
                                                    $button = __('Show Coupon', 'Couponize');
                                                    $offerlink = '#';
                                                }
                                                ?> 
                                                <a <?php if($offerlink != '') echo 'target="_blank"';?> href="<?php echo $offerlink; ?>" class="input button red secondary responsive toggle_coupon">
                                                    <?php echo $button;?>
                                                </a>
                                                <span class="input button border high secondary responsive hidden_coupon">
                                                    <?php echo $code;?>
                                                </span>
                                            <?php } 
                                            elseif($url != '') {
                                                $offerlink = home_url('/') . '?visit-offer=' . $post->ID; ?>
                                                <a target="_blank" href="<?php echo $offerlink; ?>" class="input button blue secondary responsive">
                                                    <?php _e('Visit offer', 'Couponize');?>
                                                </a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                        <?php get_template_part('includes/pagination');
                        else : ?>
                            <p class="text">
                                <?php 
                                    _e('No results match your search criteria ', 'Couponize');
                                    echo '<span class="orange">' . get_search_query() . '</span>. '; 
                                    _e('Try again.', 'Couponize');
                                ?>
                            </p> 
                            <?php
                        endif; wp_reset_query();?>
                    <div style="clear: both"></div>
                </div>
                                    
            </div>
                
            <?php get_sidebar();?> 
        </div>
    </div>
            
</div>
<?php get_footer();?>